<?php
// Heading
$_['heading_title']     = 'Відгуки магазину';

// Text
$_['text_success']      = 'Ви змінили відгуки магазину!';
$_['text_list']         = 'Список відгуків магазину';
$_['text_add']          = 'Додати відгук';
$_['text_edit']         = 'Редагувати відгук';

$_['text_load_message'] = 'Отримати повідомлення';
$_['text_retry']        = 'Повторити';

// Column
$_['column_author']     = 'Автор';
$_['column_rating']     = 'Рейтинг';
$_['column_status']     = 'Статус';
$_['column_date_added'] = 'Дата додавання';
$_['column_action']     = 'Дії';

// Entry
$_['entry_author']      = 'Автор';
$_['entry_rating']      = 'Рейтинг';
$_['entry_status']      = 'Статус';
$_['entry_text']        = 'Текст';
$_['entry_date_added']  = 'Дата додавання';

// Help

// Error
$_['error_permission']  = 'Увага: У вас немає дозволу на зміну думки магазин!';
$_['error_author']      = 'Автор повинен бути в межах від 3 до 64 символів!';
$_['error_text']        = 'Текст відгуку повинен бути принаймні 1 символ!';
$_['error_rating']      = 'Оцінка відгуку потрібно!';
$_['error_no_message']  = 'Немає нових повідомлень';


