<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Heading
$_['button_coupon'] = 'Використати';

// Text
$_['text_success']  = 'Ви використали код купону!';

// Entry
$_['entry_coupon']  = 'Введіть ваш промокод тут';

// Error
$_['error_coupon']  = 'Код купону неправильний, прострочений або перевищив кількість використань!';
$_['error_empty']   = 'Введіть код купону!';