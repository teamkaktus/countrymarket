<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Heading
$_['heading_title']     = 'Пошук';
$_['heading_tag']		= 'Тег - ';

// Text
$_['text_search']       = 'Продукти, які відповідають критеріям пошуку';
$_['text_keyword']      = 'Ключові слова';
$_['text_category']     = 'Всі категорії';
$_['text_sub_category'] = 'Пошук в підкатегоріях';
$_['text_empty']        = 'Немає продуктів які б відповідали критеріям пошуку.';
$_['text_quantity']     = 'Кількість:';
$_['text_manufacturer'] = 'Бренд:';
$_['text_model']        = 'Артикул:';
$_['text_points']       = 'Бонусні бали:';
$_['text_price']        = 'Ціна:';
$_['text_tax']          = 'Без ПДВ:';
$_['text_reviews']      = 'Згідно з %s оглядами.';
$_['text_compare']      = 'Порівняння товару (%s)';
$_['text_sort']         = 'Сортувати за:';
$_['text_default']      = 'За замовчуванням';
$_['text_name_asc']     = 'Ім`я (A - Я)';
$_['text_name_desc']    = 'Ім`я (Я - A)';
$_['text_price_asc']    = 'по зростанню';
$_['text_price_desc']   = 'по спаданню';
$_['text_rating_asc']   = 'Рейтинг (Низький)';
$_['text_rating_desc']  = 'Рейтинг (Високий)';
$_['text_model_asc']    = 'Модель (A - Я)';
$_['text_model_desc']   = 'Модель (Я - A)';
$_['text_limit']        = 'Показати:';
$_['filt_name']         = 'Фільтри:';
$_['text_weight']       = 'Вага:';
$_['text_category']     = 'Категорія:';
$_['more_shares']       = "Більше акцій";
$_['the_best_range']    = "Кращий асортимент";
$_['healthy_eating']    = "Здорове харчування";
$_['loyalty_program']   = "Програма лояльності";
$_['why_us']            = "Чому ми?";

// Entry
$_['entry_search']      = 'Фільтри';
$_['entry_description'] = 'Пошук в описі товару';
$_['text_cg']           = 'кг';