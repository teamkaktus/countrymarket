<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Text
$_['text_refine']       = 'Уточнити пошук';
$_['text_product']      = 'Товари';
$_['text_error']        = 'Категорія не знайдена!';
$_['text_empty']        = 'В цій категорії немає товарів.';
$_['text_quantity']     = 'Кількість:';
$_['text_manufacturer'] = 'Бренд:';
$_['text_model']        = 'Артикул:';
$_['text_points']       = 'Бонусні бали:';
$_['text_price']        = 'ціна:';
$_['text_tax']          = 'Без ПДВ:';
$_['text_compare']      = 'Порівняти товари (%s)';
$_['text_sort']         = 'Сортувати за:';
$_['text_default']      = 'За замовчуванням';
$_['text_name_asc']     = 'Ім`я (А - Я)';
$_['text_name_desc']    = 'Ім`я (Я - A)';
$_['text_price_asc']    = 'по зростанню';
$_['text_price_desc']   = 'по спаданню';
$_['text_rating_asc']   = 'Рейтинг (Низький)';
$_['text_rating_desc']  = 'Рейтинг (Високий)';
$_['text_model_asc']    = 'Модель (A - Я)';
$_['text_model_desc']   = 'Модель (Я - A)';
$_['text_limit']        = 'Показати:';
$_['filt_name']        = 'Фільтри:';
$_['text_weight']       = 'вага:';
$_['more_shares']           = "Більше акцій";
$_['the_best_range']           = "Кращий асортимент";
$_['healthy_eating']           = "Здорове харчування";
$_['loyalty_program']           = "Програма лояльності";
$_['why_us']           = "Чому ми?";
$_['basket']           = "Кошик";
$_['services']           = "Послуга";