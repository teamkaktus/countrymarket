<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Text
$_['text_items']          = '%s товар(ів) - %s';
$_['text_empty']          = 'Ваш кошик порожній!';
$_['text_cart']           = 'Переглянути кошик';
$_['text_checkout']       = 'Оформити замовлення';
$_['text_recurring']      = 'Профіль оплати';
$_['text_shopping_cart']  = 'Кошик';
$_['cart_total_price']    = 'Загальна вартість:';
$_['text_number']         = 'Кількість:';
$_['text_weight']         = 'Вага:';
$_['text_delivery']       = 'Доставка:';
$_['text_commission']     = 'Комісія:';
$_['text_amount']         = 'Сума:';