<?php
// Text
$_['text_refine']       = 'Refine Search';
$_['text_product']      = 'Products';
$_['text_error']        = 'Category not found!';
$_['text_empty']        = 'There are no products to list in this category.';
$_['text_quantity']     = 'Qty:';
$_['text_manufacturer'] = 'Brand:';
$_['text_model']        = 'Product Code:';
$_['text_points']       = 'Reward Points:';
$_['text_price']        = 'price:';
$_['text_tax']          = 'Ex Tax:';
$_['text_compare']      = 'Product Compare (%s)';
$_['text_sort']         = 'Sort By:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_price_asc']    = 'ascending';
$_['text_price_desc']   = 'descending';
$_['text_rating_asc']   = 'Rating (Lowest)';
$_['text_rating_desc']  = 'Rating (Highest)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_limit']        = 'Show:';
$_['filt_name']        = 'Filters:';
$_['text_weight']       = 'weight:';
$_['more_shares']           = "More shares";
$_['the_best_range']           = "The best range";
$_['healthy_eating']           = "Healthy eating";
$_['loyalty_program']           = "Loyalty program";
$_['why_us']           = "Why us?";
$_['basket']           = "Basket";
$_['services']           = "Service";