<?php
// Heading
$_['heading_title']     = 'Search';
$_['heading_tag']		= 'Tag - ';

// Text
$_['text_search']       = 'Products meeting the search criteria';
$_['text_keyword']      = 'Keywords';
$_['text_category']     = 'All Categories';
$_['text_sub_category'] = 'Search in subcategories';
$_['text_empty']        = 'There is no product that matches the search criteria.';
$_['text_quantity']     = 'Qty:';
$_['text_manufacturer'] = 'Brand:';
$_['text_model']        = 'Product Code:';
$_['text_points']       = 'Reward Points:';
$_['text_price']        = 'Price:';
$_['text_tax']          = 'Ex Tax:';
$_['text_reviews']      = 'Based on %s reviews.';
$_['text_compare']      = 'Product Compare (%s)';
$_['text_sort']         = 'Sort By:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_price_asc']    = 'ascending';
$_['text_price_desc']   = 'descending';
$_['text_rating_asc']   = 'Rating (Lowest)';
$_['text_rating_desc']  = 'Rating (Highest)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_limit']        = 'Show:';
$_['filt_name']         = 'Filters:';
$_['text_weight']       = 'Weight:';
$_['more_shares']       = "More shares";
$_['the_best_range']    = "The best range";
$_['healthy_eating']    = "Healthy eating";
$_['loyalty_program']   = "Loyalty program";
$_['why_us']            = "Why us?";
$_['text_category']     = 'Category:';

// Entry
$_['entry_search']      = 'Criteria';
$_['entry_description'] = 'Search in product descriptions';
$_['text_cg']           = 'kg';