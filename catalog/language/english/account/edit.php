<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Heading
$_['heading_title']      = 'Account';
$_['heading_title1']     = 'Profile';
$_['heading_zam']        = 'Ordering';
$_['heading_spov']       = 'Notice';
$_['heading_kyp']        = 'Coupons';
$_['save']               = 'Save';
$_['entry_password']     = 'Enter the password';
$_['entry_confirm']      = 'Confirm the password';
$_['text_home']         = 'Home';

// Text
$_['text_account']       = 'Account';
$_['text_edit']          = 'To change the information';
$_['text_your_details']  = 'Your personal data';
$_['text_success']       = 'You have successfully changed your account.';
$_['text_empty']         = 'You have no pre-orders!';

// Entry
$_['entry_firstname']    = 'Name';
$_['entry_lastname']     = 'Last name';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Phone';
$_['entry_fax']          = 'Fax';

// Error
$_['error_exists']       = 'This E-Mail address is already registered!';
$_['error_firstname']    = 'The name must contain between 1 and 32 characters!';
$_['error_lastname']     = 'Last name should contain between 1 and 32 characters!';
$_['error_email']        = 'Wrong E-Mail!';
$_['error_telephone']    = 'Phone must be between 3 and 32 characters!';
$_['error_custom_field'] = '%s is required!';