<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Vouchers';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Powered By <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';
$_['delivery']           = "Delivery";
$_['about_us']           = "About us";
$_['actions']           = "Actions";
$_['contacts']           = "Contacts";
$_['reviews']           = "Reviews";
$_['we_are_in_social_networks']           = "We are in social networks:";
$_['we_accept']           = "We accept:";
$_['footer_text']           = "All rights reserved (c) 2016";
$_['development_and_support']           = "Development:";