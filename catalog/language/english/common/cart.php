<?php
// Text
$_['text_items']          = '%s item(s) - %s';
$_['text_empty']          = 'Your shopping cart is empty!';
$_['text_cart']           = 'View Cart';
$_['text_checkout']       = 'Checkout';
$_['text_recurring']      = 'Payment Profile';
$_['text_shopping_cart']  = 'Вasket';
$_['text_number']         = 'Number:';
$_['text_weight']         = 'Weight:';
$_['text_delivery']       = 'Delivery:';
$_['text_commission']     = 'Commission:';
$_['text_amount']         = 'Amount:';
$_['cart_total_price']    = 'The total cost:';