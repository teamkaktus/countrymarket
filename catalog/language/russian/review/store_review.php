<?php
// Text
$_['heading_title']	= 'Отзывы';

$_['text_write']               = 'Write a review';
$_['text_login']               = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';
$_['text_no_reviews']          = 'There are no reviews.';
$_['text_note']                = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_success']             = 'Thank you for your review. It has been submitted to the webmaster for approval.';
$_['reviews_about_us']         = 'Отзывы о нас';
$_['text_home']         = 'Главная';

// Entry
$_['entry_name']               = 'Ваше имя';
$_['entry_review']             = 'Ваши отзыв';
$_['entry_mail']               = 'Ваш Mail';
$_['entry_rating']             = 'Рейтинг';
$_['entry_good']               = 'Хорошо';
$_['entry_bad']                = 'Плохо';
$_['send']                     = 'Оставить отзыв';

// Error
$_['error_name']               = 'Warning: Review Name must be between 3 and 25 characters!';
$_['error_text']               = 'Warning: Review Text must be between 25 and 3000 characters!';
$_['error_rating']             = 'Warning: Please select a review rating!';
$_['error_captcha']            = 'Warning: Verification code does not match the image!';

// Ошибка
$_[ 'error_name'] = 'Предупреждение: Обзор Имя должно быть от 3 до 25 символов!';
$_[ 'error_mail'] = 'Внимание: Mail';
$_[ 'error_text'] = 'Предупреждение: Текст отзыва должен быть от 25 до 3000 символов!';
$_[ 'error_rating'] = 'Внимание: Пожалуйста, выберите оценку обзора';
$_[ 'error_captcha'] = 'Внимание: Проверочный код не совпадает с изображением!';

