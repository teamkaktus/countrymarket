<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Heading
$_['heading_title']      = 'Аккаунт';
$_['heading_title1']     = 'Профайл';
$_['heading_zam']        = 'Заказ';
$_['heading_spov']       = 'Уведомление';
$_['heading_kyp']        = 'Купоны';
$_['save']               = 'Сохранить';
$_['entry_password']     = 'Введите пароль';
$_['entry_confirm']      = 'Подтвердите пароль';
$_['text_home']         = 'Главная';

// Text
$_['text_account']       = 'Учетная запись';
$_['text_edit']          = 'Изменить информацию';
$_['text_your_details']  = 'Ваши личные данные';
$_['text_success']       = 'Вы успешно сменили учетную запись.';
$_['text_empty']         = 'У Вас нет предварительных заказов!';

// Entry
$_['entry_firstname']    = 'Имя';
$_['entry_lastname']     = 'Фамилия';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Телефон';
$_['entry_fax']          = 'Факс';

// Error
$_['error_exists']       = 'Такая E-Mail адрес уже зарегистрирован!';
$_['error_firstname']    = 'Имя должно содержать от 1 до 32 символов!';
$_['error_lastname']     = 'Фамилия должно содержать от 1 до 32 символов!';
$_['error_email']        = 'Неправильный E-Mail!';
$_['error_telephone']    = 'Телефон должен содержать от 3 до 32 символов!';
$_['error_custom_field'] = '%s необходимо!';