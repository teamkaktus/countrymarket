<?php
// Heading
$_['heading_title']     = 'Поиск';
$_['heading_tag']		= 'По тегу - ';

// Text
$_['text_search']       = 'Результаты поиска';
$_['text_keyword']      = 'Ключевые слова';
$_['text_category']     = 'Все категории';
$_['text_sub_category'] = 'Поиск в подкатегориях';
$_['text_empty']        = 'Нет товаров, которые соответствуют критериям поиска.';
$_['text_quantity']     = 'Количество:';
$_['text_manufacturer'] = 'Производитель:';
$_['text_model']        = 'Модель:';
$_['text_points']       = 'Бонусные баллы:';
$_['text_price']        = 'Цена:';
$_['text_tax']          = 'Без НДС:';
$_['text_reviews']      = 'Всего отзывов: %s';
$_['text_compare']      = 'Сравнение товаров (%s)';
$_['text_sort']         = 'Сортировка:';
$_['text_default']      = 'По умолчанию';
$_['text_name_asc']     = 'Название (А - Я)';
$_['text_name_desc']    = 'Название (Я - А)';
$_['text_price_asc']    = 'по возрастанию';
$_['text_price_desc']   = 'по убыванию';
$_['text_rating_asc']   = 'Рейтинг (начиная с низкого)';
$_['text_rating_desc']  = 'Рейтинг (начиная с высокого)';
$_['text_model_asc']    = 'Модель (А - Я)';
$_['text_model_desc']   = 'Модель (Я - А)';
$_['text_limit']        = 'Показать:';
$_['filt_name']         = 'Фильтры:';
$_['text_weight']       = 'Вес:';
$_['more_shares']       = "Больше акций";
$_['the_best_range']    = "Лучший ассортимент";
$_['healthy_eating']    = "Здоровое питание";
$_['loyalty_program']   = "Программа лояльности";
$_['why_us']            = "Почему мы?";
$_['text_category']     = 'Категория:';

// Entry
$_['entry_search']      = 'Поиск:';
$_['entry_description'] = 'Искать в описании товаров';
$_['text_cg']           = 'кг';
