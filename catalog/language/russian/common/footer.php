<?php
// Text
$_['text_information']  = 'Информация';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Обратная связь';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнерская программа';
$_['text_special']      = 'Акции';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Закладки';
$_['text_newsletter']   = 'Рассылка';
$_['text_powered']      = 'Работает на <a href="http://opencart-russia.ru">OpenCart "Русская сборка"</a><br /> %s &copy; %s';
$_['delivery']           = "Доставка";
$_['about_us']           = "О нас";
$_['actions']            = "Акции";
$_['reviews']            = "Отзывы";
$_['contacts']           = "Контакты";
$_['we_are_in_social_networks']           = "Мы в социальных сетях:";
$_['we_accept']           = "Принимаем:";
$_['footer_text']           = "Все права защищены (с) 2016";
$_['development_and_support']           = "Разработка:";