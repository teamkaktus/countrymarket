<?php
// Text
$_['text_home']          = 'Главная';
$_['text_wishlist']      = 'Закладки (%s)';
$_['text_shopping_cart'] = 'Корзина';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'Транзакции';
$_['text_download']      = 'Загрузки';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Смотреть Все';
$_['language_selection'] = "Язык:";
$_['select_country']     = "Страна:";
$_['delivery']           = "Доставка";
$_['about_us']           = "О нас";
$_['actions']            = "Акции";
$_['contacts']           = "Контакты";
$_['reviews']            = "Отзывы";
$_['payment']            = "Оплата";
$_['quality_control']            = "Контроль качества";
$_['offers']            = "Предложения";
$_['default_stor_name']  = "Украина";
$_['header_profile_text']= "Приветствуєм";
