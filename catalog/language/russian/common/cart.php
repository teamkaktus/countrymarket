<?php
// Text
$_['text_items']          = 'Товаров %s (%s)';
$_['text_empty']          = 'Ваша корзина пуста!';
$_['text_cart']           = 'Перейти в корзину';
$_['text_checkout']       = 'Оформить';
$_['text_recurring']      = 'Платежный профиль';
$_['text_shopping_cart']  = 'Корзина';
$_['text_number']         = 'Количество';
$_['text_weight']         = 'Вес';
$_['text_delivery']       = 'Доставка';
$_['text_commission']     = 'Комиссия';
$_['text_amount']         = 'Сума:';
$_['cart_total_price']    = 'Общая стоимость:';