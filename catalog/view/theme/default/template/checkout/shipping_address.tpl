
<div class="col-md-12 checkout_form" id="chekingform">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group required">
							<label for=""><?php echo $entry_firstname; ?></label>
							<input type="text" class="form-control" name="firstname"  <?php if($shipping_address['firstname']){ ?> value="<?php echo $shipping_address['firstname']; ?>"  <?php } ?> >
							<?php if($shipping_address['address_id']){   ?>
							 <input type="text" class="hidden form-control" name="address_id" value="<?php echo $shipping_address['address_id']; ?>">
							  <?php } ?>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group required">
							<label for=""><?php echo $entry_lastname; ?> </label>
							<input type="text" class="form-control" name="lastname" <?php if($shipping_address['lastname']){ ?> value="<?php echo $shipping_address['lastname']; ?>"  <?php } ?> >
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group required">
							<label for=""><?php echo $entry_zone; ?></label>
						   <input type="text" class="form-control" name="city"  <?php if($shipping_address['city']){ ?> value="<?php echo $shipping_address['city']; ?>"  <?php } ?>>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group required">
							<label for=""><?php echo $entry_address_1; ?></label>
							<input type="text" class="form-control" name="address" <?php if($shipping_address['address_1']){ ?> value="<?php echo $shipping_address['address_1']; ?>"  <?php } ?>>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group required">
							<label for=""><?php echo $entry_telephone; ?></label>
							<input type="tel" class="form-control" name="address_2" <?php if($shipping_address['address_2']){ ?> value="<?php echo $shipping_address['address_2']; ?>"  <?php } ?>>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group required">
							<label for=""><?php echo $entry_email; ?> </label>
							<input type="email" class="form-control" name="company" <?php if($shipping_address['company']){ ?> value="<?php echo $shipping_address['company']; ?>"  <?php } ?>>
						</div>
					</div>
					<div class="col-md-4">
						
                          <p><?php echo $text_payment_method; ?></p>
                        <?php foreach ($payment_methods as $payment_method) { ?>
                    <div class="radio">
                          <label>
                            <?php if ($payment_method['code'] == $code || !$code) { ?>
                            <?php $code = $payment_method['code']; ?>
                            <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" checked="checked" />
                            <?php echo $payment_method['title']; ?>
                            <?php if ($payment_method['terms']) { ?>
                            (<?php echo $payment_method['terms']; ?>)
                            <?php } ?>
                            <?php }  ?>
                            
                          </label>
                        </div>
                        <?php } ?>
                       
					</div>
				</div>
				<br />
				<div class="row checkbox-row">
					<div class="col-md-12 checkbox-header">
						<?php echo $osob; ?>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="radio" name="special_notes" id="health_problems" value="1" >
							<label for="health_problems"><?php echo $bad1; ?></label>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="radio" name="special_notes" id="poor_hearing"  value="2" >
							<label for="poor_hearing"><?php echo $bad2; ?></label>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="radio" name="special_notes" id="poor_sees" value="3" >
							<label for="poor_sees"><?php echo $bad3; ?></label>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="radio" name="special_notes" id="not_walking" value="4" >
							<label for="not_walking"><?php echo $bad4; ?> </label>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="radio" name="special_notes" id="other_defects" value="5" >
							<label for="other_defects"><?php echo $bad5; ?></label>
						</div>
					</div>
				</div>
				<div class="row checkbox-row">
					<div class="col-md-12 checkbox-header">
						<?php echo $iak; ?>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="radio" name="referral" id="social_networks" value="1" >
							<label for="social_networks"><?php echo $iak1; ?></label>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="radio" name="referral" id="search_sites" value="2" >
							<label for="search_sites"><?php echo $iak2; ?></label>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="radio" name="referral" id="r_advertising" value="3" >
							<label for="r_advertising"><?php echo $iak3; ?></label>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="radio" name="referral" id="friends" value="4" >
							<label for="friends"><?php echo $iak4; ?></label>
						</div>
					</div>
				</div>
			


			
	
			</div>

  <div class="buttons clearfix">
    <div class="pull-right">
      <input value="<?php echo $button_continue; ?>" id="button-shipping-address" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
    </div>
  </div>

