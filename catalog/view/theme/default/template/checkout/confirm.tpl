<?php if (!isset($redirect)) { ?>

<div class="table-responsive">
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <td class="text-left"><?php echo $column_name; ?></td>
        <td class="text-left"><?php echo $column_model; ?></td>
        <td class="text-right"><?php echo $column_quantity; ?></td>
        <td class="text-right"><?php echo $column_price; ?></td>
        <td class="text-right"><?php echo $column_total; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($products as $product) { ?>
      <tr>
        <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
          <?php foreach ($product['option'] as $option) { ?>
          <br />
          &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
          <?php } ?>
          <?php if($product['recurring']) { ?>
          <br />
          <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
          <?php } ?></td>
        <td class="text-left"><?php echo $product['model']; ?></td>
        <td class="text-right"><?php echo $product['quantity']; ?></td>
        <td class="text-right"><?php echo $product['price']; ?></td>
        <td class="text-right"><?php echo $product['total']; ?></td>
      </tr>
      <?php } ?>
      <?php foreach ($vouchers as $voucher) { ?>
      <tr>
        <td class="text-left"><?php echo $voucher['description']; ?></td>
        <td class="text-left"></td>
        <td class="text-right">1</td>
        <td class="text-right"><?php echo $voucher['amount']; ?></td>
        <td class="text-right"><?php echo $voucher['amount']; ?></td>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
      
      <tr>
        <td colspan="4" class="text-right"><strong><?= $cart_number_of_products_text; ?>:</strong></td>
        <td class="text-right"><?=$count; ?></td>
      </tr>
      <tr>
        <td colspan="4" class="text-right"><strong><?= $cart_weight_text; ?>:</strong></td>
        <td class="text-right"><?=$weight; ?></td>
      </tr>
      <tr>
        <td colspan="4" class="text-right"><strong><?= $parcel_count_text; ?>:</strong></td>
        <td class="text-right"><?php foreach($parcel as $item){ ?>
									<span><?=$item['count'];?> - <?=$item['price'];?> | </span>
									<?php } ?></td>
      </tr>
      <tr>
        <td colspan="4" class="text-right"><strong><?= $parcel_count_text2; ?>:</strong></td>
        <td class="text-right"><?=$parcelpos; ?> $</td>
      </tr>
      <tr>
        <td colspan="4" class="text-right"><strong><?= $cart_cost_text; ?>:</strong></td>
        <td class="text-right"><?=$totals; ?></td>
      </tr>
      <tr>
        <td colspan="4" class="text-right"><strong><?= $cart_commission_text; ?>:</strong></td>
        <td class="text-right"><?=$commission; ?> $</td>
      </tr>
      <tr>
        <td colspan="4" class="text-right"><strong><?= $cart_commission_text2; ?>:</strong></td>
        <td class="text-right"><?=$commissionpos; ?> $</td>
      </tr>
      <tr>
        <td colspan="4" class="text-right"><strong><?= $cart_the_total_cost_text; ?>:</strong></td>
        <td class="text-right"><?=$totalsUsd;?></td>
      </tr>
      
    </tfoot>
  </table>
  <div class="row">
					<div class="col-md-6">
						<div class="panel-group coupon" id="accordion">
							<?php echo $coupon; ?>
						</div>
					</div>
					<div class="col-md-6 no-padding btn-row">
						<div class="clearfix"></div>
					</div>
				</div>
</div>
<?php echo $payment; ?>

<?php } else { ?>
<script type="text/javascript"><!--
//location = '<?php echo $redirect; ?>';
//--></script>
<?php } ?>
