<?php echo $header; ?>
<div style="display: none">
	<audio id="beep-one" controls="controls" preload="auto" style="    position: fixed; visibility: hidden;">
		<source src="/image/yvd.mp3"></source>
	</audio>
</div>
<div class="container" xmlns="http://www.w3.org/1999/html">
	<ul class="breadcrumbmy breadcrumbmy-margin-style" >
		<div class="row">
		<?php
      $i = 1;
      $count = count($breadcrumbs);
    ?>
		<div class="col-lg-5 col-md-5 col-sm-7" style="padding-left: 0; padding-right: 0;">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<?php if($count == $i){ ?>
			<li><a class="breadcrumbmy-last-elem"><?php echo $breadcrumb['text']; ?></a></li>
			<?php }else{ ?>
			<li><a class="breadcrumbmy-first-elem" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'].' - '; ?></a></li>
			<?php } ?>
			<?php $i+=1; ?>
			<?php } ?>
		</div>
		<div class="col-lg-7 col-md-7 col-sm-5 hidden-xs" style="padding-left: 0; margin-top: 8px;">
			<span class="legend__right-information l-ab_villa" style="width: 100%"></span>
		</div>
		</div>
	</ul>
	<?php if ($error_warning) { ?>
	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	<?php } ?>
	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="contentc" class="<?php echo $class; ?>">
		<div id="contentcart" style="box-shadow: 0 0 5px 1px rgba(0, 0, 0, 0.3);">
		<?php echo $content_top; ?>
			<!--<p class="baskt_art checkout_title text-center"><?php echo $heading_title; ?></p>-->
			<div class="panel panel-default">
				<div class="cart-part1-div-style">
					<h4 class="cart-part1-h4-style"><?php echo $text_checkout_option1; ?></h4>
				</div>
				<div class="row checkoutprodheader padleft30 hidden-xs">
					<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-center text_style_checkout"><?=$column_name;?></div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center text_style_checkout"><?=$column_model;?></div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center text_style_checkout"><?=$column_quantity;?></div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center text_style_checkout"><?=$column_price;?></div>
					<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center"></div>
				</div>
				<?php foreach ($products as $product) { ?>

				<div class="row checkoutprodheader padleft30">
					<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
						<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 text-center">
						<a href="<?php echo $product['href']; ?>">
							<img style="width: 70px; margin-top: -17px;" src="<?php echo $product['thumb']; ?>"
								 alt="<?php echo $product['name']; ?>"
								 title="<?php echo $product['name']; ?>" class="img-responsive"/>
						</a>
						</div>
						<div class="col-xs-6 col-sm-8 col-md-8 col-lg-8">
						<h4>
							<a class="cart-name_style_checkout"
							   href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?>
							</a>
						</h4>
						</div></div>
					<div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 text-center" style="margin-top: 8px;">
						<span class="price cart-price_checkout"><?php echo $product['price']; ?></span>
					</div>
					<div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 text-center" style="position: relative;">
						<img src="../image/fygura-47.png">
						<div class="row cart-quantity-div-style">
						<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no_padding cart-style_charges">
							<i class="cart-charges_2 col-xs-12 col-sm-12 col-md-12 col-lg-12 no_padding"  onclick="quantity_dec(<?=$product['key'];?>, <?=$product['product_id'];?>)">-</i>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no_padding cart-style_quantity margin_quantity" style="margin-left: 0 !important;">
							<input style="border: 0; background-color: rgba(0, 0, 0, 0); outline: none" type="text" name="quantity[<?php echo $product['key']; ?>]"
								   value="<?php echo $product['quantity']; ?>" size="1" class="text-center input-quantity" oninput="quantity_onchange(<?=$product['key'];?>, <?=$product['product_id'];?>)"/>
						</div>
						<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 no_padding cart-style_charges">
							<i class="cart-charges_1 col-xs-12 col-sm-12 col-md-12 col-lg-12 no_padding"   onclick="quantity_inc(<?=$product['key'];?>, <?=$product['product_id'];?>)">+</i>
						</div>
						</div>
					</div>
					<div class="col-xs-3 col-sm-2 col-md-2 col-lg-2 text-center cart-price_checkout" style="margin-top: 8px;">
						<?php echo $product['total']; ?>
					</div>
					<div class="col-xs-3 col-sm-1 col-md-1 col-lg-1 text-left" style="margin-top: 8px;">
						<span class="remove-product remove-icon-style"
															type="button" data-toggle="tooltip"
															title=""
															onclick="cart.remove('<?php echo $product['key']; ?>');">
										X
						</span>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-10 col-sm-11 col-md-11 col-lg-11 checkoutprodborder cart-aftr-line-style"></div>
				</div>

				<?php } ?>

				<div class="row no-margin trash-information padleft30">
					<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5 container_KP_40">
						<div class="col-xs-6 col-sm-7 col-md-6 col-lg-6 text-left no-padding">
							<div class="cart-text_style_checkout row no-padding padbot"><?= $cart_number_of_products_text; ?>:</div>
							<div class="cart-text_style_checkout row no-padding padbot"><?= $cart_weight_text; ?>:</div>
							<div class="cart-text_style_checkout row no-padding padbot"><?= $parcel_count_text; ?>:</div>
							<?php if($parcelpos > 0){ ?>
							<div class="cart-text_style_checkout row no-padding padbot"><?= $parcel_count_text2; ?>:</div>
							<?php } ?>

						</div>
						<div class="col-xs-6 col-sm-5 col-md-5 col-lg-5 text-left no-padding">
							<div class="cart-text_style_checkout row no-padding padbot"><span id="cartCount"><?=$quantitytotal; ?> шт.</span></div>
							<div class="cart-text_style_checkout row no-padding padbot"><span id="totalWeight"><?=$weight; ?> кг.</span></div>
							<div class="cart-text_style_checkout row no-padding padbot">
								<div id="parcel">
									<?php $i=0; foreach($parcel as $item){ ?>
									<span><?=$item['count'];?> - <?=$item['price'];?>
										<?php if($i>1){ ?>  |<?php } ?>
									</span>
									<?php $i++; } ?>
								</div>
							</div>
							<?php if($parcelpos > 0){ ?>
							<div class="row no-padding padbot"><span id="totalWeight"><?=$parcelpos; ?></span></div>
							<?php } ?>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
						<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5 text-left no-padding">
							<div class="cart-text_style_checkout row no-padding padbot"><?= $cart_cost_text; ?>:</div>
							<!--<div class="row no-padding padbot"><?= $cart_delivery_text; ?>:</div>-->
							<div class="cart-text_style_checkout row no-padding padbot"><?= $cart_commission_text; ?>:</div>
							<?php if($commissionpos > 0){ ?>
							<div class="cart-text_style_checkout row no-padding padbot"><?= $cart_commission_text2; ?>:</div>
							<?php } ?>
							<div class="cart-text_style_checkout-total row no-padding padbot"><?= $cart_the_total_cost_text; ?>:</div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-7 col-lg-7 text-left no-padding">
							<div class="cart-text_style_checkout row no-padding padbot"><span id="totalPrice"><?=$totals; ?></span></div>
							<!--<div class="row no-padding padbot"><b><span id="deliveryPrice"><?=$delivery; ?></span></b></div>-->
							<div class="cart-text_style_checkout row no-padding padbot"><span id="commission"><?=$commission; ?> $</span></div>
							<?php if($commissionpos > 0){ ?>
							<div class="cart-text_style_checkout row no-padding padbot"><span id="commission"><?=$commissionpos; ?> $</span></div>
							<?php } ?>
							<div class="cart-text_style_checkout-total row no-padding padbot"><span id="allTotalPrice"><?=$totalsUsd;?> $</span></div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="row row text-center">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<?php echo $coupon; ?>
					</div>
					<div class="hidden-xs col-sm-12 col-md-12 col-lg-12 text-center">
						<input style=" margin-left: 0px; margin-top: 15px; margin-bottom: 15px; border-radius: 0;" type="button" value="<?php echo $button_continue; ?>" id="button-loginshov" data-loading-text="<?php echo $text_loading; ?>" class="btn cart-button_style2-style " />
					</div>

                    <div class="col-xs-12 hidden-sm hidden-md hidden-lg text-center">
						<input style=" margin-left: 0px; margin-top: 47px;" type="button" value="<?php echo $button_continue; ?>" id="button-loginshov" data-loading-text="<?php echo $text_loading; ?>" class="btn cart-button_style2-style " />
					</div>
				</div>



			</div>

          <div id="login_dost" style="display: none; box-shadow: 0 0 5px 1px rgba(0, 0, 0, 0.3);">
                <?php if (!$logged && $account != 'guest') { ?>
				<div class="panel panel-default" >
					<div class="cart-part1-div-style">
						<h4 class="cart-part1-h4-style"><?php echo $text_checkout_option2; ?></h4>
					</div>
					 <div class="row">
                      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                       <div class="checkoutpadding">
                        <h2 class="text-center cart-new-customer-style"><?php echo $text_new_customer; ?></h2>
                        <div class="radio text-left cart-text_register_checkout">
                          <label style="padding-left: 0; font-size: 14px;">
                            <input style="visibility: hidden;" type="radio" name="account" value="register" checked="checked" />
                            <?php echo $text_register; ?></label>
                            </div>
                            <p class="cart-text_qwe_checkout"><?php echo $text_register_account; ?></p>
                        <input style="margin-left: 0px; margin-top: 55px; border-radius: 0;" type="button" value="<?php echo $button_continue; ?>" id="button-account" data-loading-text="<?php echo $text_loading; ?>" class="btn cart-cart-remember-password-style" />
                      </div>
                      </div>
                      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="logininput">
                    <h2 class="text-center cart-new-customer-style"><?php echo $text_returning_customer; ?></h2>
                    <p class="text-left cart-text_register_checkout" style="padding-left: 40px"><?php echo $text_i_am_returning_customer; ?></p>
                    <div class="checkoutpadding">
                    <div class="form-group">
      					<input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control cart-cupon-input-style" />
    				</div>
                    <div class="form-group">
                      <input  type="password" name="password" value="" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control cart-cupon-input-style" />
                      <input style="margin-left: 0px; margin-top: 30px; border-radius: 0;" type="button" value="<?php echo $button_login; ?>" id="button-login" data-loading-text="<?php echo $text_loading; ?>" class="btn cart-button_style2-style-login" />
						<a class="cart-remember-password-style" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
					</div>
                  </div>
					  </div>
                     </div>
				</div>
				<?php } ?>
				<?php if ($logged && $account != 'guest') { ?>

				<div class="panel panel-default">
					<div class="cart-part1-div-style">
						<h4 class="cart-part1-h4-style"><?php echo $text_checkout_payment_address; ?></h4>
					</div>
						<div class="panel-body">
						    <div style="margin-bottom: 10px;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 checkout_form" id="chekingform">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart-checkbox-header">
										Інформація про відправника					</div>
									<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
										<div class="form-group required">
											<label class="style_checkout_label" for=""></label>
											<input type="text" class="form-control form-control_style_checkout_2 cart-step3-input-style" name="myfirstname"   value="<?php echo $firstname; ?>" >
										</div>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
										<div class="form-group required">
											<label class="style_checkout_label" for=""></label>
											<input type="text" class="form-control form-control_style_checkout_2 cart-step3-input-style" name="mylastname" value="<?php echo $lastname; ?>" >
										</div>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
										<div class="form-group required">
											<label class="style_checkout_label" for=""></label>
											<input type="text" class="form-control form-control_style_checkout_2 cart-step3-input-style" name="myfone"   value="<?php echo $telephone; ?>" >
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart-checkbox-header" style=" margin-top: 15px;  margin-bottom: 15px;">
										Інформація про одержувача					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group required">
							<select name="address_id" class="form-control" onchange="this.options[this.selectedIndex].value && (window.location = 'index.php?route=checkout/checkout&id='+this.options[this.selectedIndex].value);">
								<option onclick="alert('fdf');" value="" <?php if ($address_id=='') { ?>selected=""<?php } ?>><?= $entry_address_3; ?></option>
								<?php
								 var_dump($address_id);
								 foreach ($addresses as $address) { ?>
								<?php if ($address['address_id'] == $address_id) { ?>
								<option value="<?php echo $address['address_id']; ?>" selected=""><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?></option>
								<?php } else { ?>
								<option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
								<?php } ?>
								<?php } ?>
							</select>
						</div>
						<div class="form-group required">
							<label class="style_checkout_label" for=""></label>
							<input type="text" class="form-control form-control_style_checkout_2 cart-step3-input-style" name="address" placeholder="<?php echo $entry_address_1; ?>" <?php if($shipping_address['address_1']){ ?> value="<?php echo $shipping_address['address_1']; ?>"  <?php } ?>>
						</div>
					</div>

				</div>
								<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="form-group required">
							<label class="style_checkout_label" for=""></label>
							<input type="text" class="form-control form-control_style_checkout_2 cart-step3-input-style" name="firstname" placeholder="<?php echo $entry_firstname; ?>"  <?php if($shipping_address['firstname']){ ?> value="<?php echo $shipping_address['firstname']; ?>"  <?php } ?> >
							<?php if($shipping_address['address_id']){   ?>
							 <input type="text" class="hidden form-control" name="address_id" value="<?php echo $shipping_address['address_id']; ?>">
							  <?php } ?>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="form-group required">
							<label class="style_checkout_label" for=""></label>
							<input type="text" class="form-control form-control_style_checkout_2 cart-step3-input-style" name="lastname" placeholder="<?php echo $entry_lastname; ?>" <?php if($shipping_address['lastname']){ ?> value="<?php echo $shipping_address['lastname']; ?>"  <?php } ?> >
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="form-group required">
							<label class="style_checkout_label" for=""></label>
						   <input type="text" class="form-control form-control_style_checkout_2 cart-step3-input-style" name="city" placeholder="<?php echo $entry_zone; ?>"  <?php if($shipping_address['city']){ ?> value="<?php echo $shipping_address['city']; ?>"  <?php } ?>>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group required">
							<label class="style_checkout_label" for=""></label>
							<input type="text" class="form-control form-control_style_checkout_2 cart-step3-input-style" name="address" placeholder="<?php echo $entry_address_1; ?>" <?php if($shipping_address['address_1']){ ?> value="<?php echo $shipping_address['address_1']; ?>"  <?php } ?>>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="form-group required">
							<label class="style_checkout_label" for=""></label>
							<input type="tel" class="form-control form-control_style_checkout_2 cart-step3-input-style" name="address_2" placeholder="<?php echo $entry_telephone; ?>" <?php if($shipping_address['address_2']){ ?> value="<?php echo $shipping_address['address_2']; ?>"  <?php } ?>>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="form-group required">
							<label class="style_checkout_label" for=""></label>
							<input type="email" class="form-control form-control_style_checkout_2 cart-step3-input-style" placeholder="<?php echo $entry_email; ?>" name="company" <?php if($shipping_address['company']){ ?> value="<?php echo $shipping_address['company']; ?>"  <?php } ?>>

							<?php if($address_id){ ?><input type="text" class="hidden" name="address_id"  value="<?php echo $address_id; ?>"  > <?php } ?>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">


                          <p><?php echo $text_payment_method; ?></p>
                    <div class="radio">
						<?php foreach ($payment_methods as $payment_method) { ?>
						<label>
							                            <?php if ($payment_method['code'] == $code || !$code) { ?>
							                            <?php $code = $payment_method['code']; ?>
							                            <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" checked="checked" />
							                              <img src="/image/icon3.png">
							<img src="/image/icon1.png">
							<img src="/image/icon2.png">
							<?php if ($payment_method['terms']) { ?>
														  <?php echo $payment_method['terms']; ?>
							                            <?php } ?>
							                          <?php }  ?>
						</label>
						<?php } ?>


                        </div>
					</div>
					<div class="col-md-12">
						<textarea placeholder="<?php echo $entry_comment; ?>" name="custom_field" rows="3" class="form-control"></textarea>
					</div>
				</div>
				<div class="row checkbox-row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart-checkbox-header" style="margin-top: 30px; margin-bottom: 30px">
						<?php echo $osob; ?>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<input type="radio" name="special_notes" id="health_problems" value="1" >
							<label class="cart-health_problems" for="health_problems"><?php echo $bad1; ?></label>
						</div>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<input type="radio" name="special_notes" id="poor_hearing"  value="2" >
							<label class="cart-health_problems" for="poor_hearing"><?php echo $bad2; ?></label>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="form-group">
							<input type="radio" name="special_notes" id="poor_sees" value="3" >
							<label class="cart-health_problems" for="poor_sees"><?php echo $bad3; ?></label>
						</div>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<input type="radio" name="special_notes" id="not_walking" value="4" >
							<label class="cart-health_problems" for="not_walking"><?php echo $bad4; ?> </label>
						</div>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<input type="radio" name="special_notes" id="other_defects" value="5" >
							<label class="cart-health_problems" for="other_defects"><?php echo $bad5; ?></label>
						</div>
					</div>
				</div>
                                <div class="row checkbox-row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cart-checkbox-header" style="margin-bottom: 20px; margin-top: 20px">
                                        <?php echo $iak; ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group">
                                            <input type="radio" name="special_notes" id="health_problems" value="1" >
                                            <label class="cart-health_problems" for="health_problems"><?php echo $iak1; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group">
                                            <input type="radio" name="special_notes" id="poor_hearing"  value="2" >
                                            <label class="cart-health_problems" for="poor_hearing"><?php echo $iak2; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group">
                                            <input type="radio" name="special_notes" id="poor_sees" value="3" >
                                            <label class="cart-health_problems" for="poor_sees"><?php echo $iak3; ?></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group">
                                            <input type="radio" name="special_notes" id="not_walking" value="4" >
                                            <label class="cart-health_problems" for="not_walking"><?php echo $iak4; ?> </label>
                                        </div>
                                    </div>
                                </div>
			</div>

  <div class=" clearfix col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 40px;">
    <div class="text-center">
      <input type="button" style="border-radius: 0;" value="<?php echo $text_btn_and; ?>" id="button-shipping-address" data-loading-text="<?php echo $text_loading; ?>" class="btn cart-button_style_cart " />
    </div>
  </div>


						</div>

				</div>
				<?php } ?>
		  </div>
				<?php if ($shipping_required) { ?>

				<div class="panel panel-default hidden">
					<div class="panel-heading">
						<h4 class="panel-title"><?php echo $text_checkout_shipping_method; ?></h4>
					</div>
					<div class="panel-collapse collapse" id="collapse-shipping-method">
						<div class="panel-body"></div>
					</div>
				</div>
				<?php } ?>
				<div class="panel panel-default hidden">
					<div class="panel-heading">
						<h4 class="panel-title"><?php echo $text_checkout_payment_method; ?></h4>
					</div>
					<div class="panel-collapse collapse" id="collapse-payment-method">
						<div class="panel-body"></div>
					</div>
				</div>
			<div id="conf" style="display: none">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"><?php echo $text_checkout_confirm; ?></h4>
					</div>
					<div class="panel-collapse collapse" id="collapse-checkout-confirm">
						<div class="panel-body"></div>
					</div>
				</div>
			</div>
			<?php echo $content_bottom; ?><div></div>
		<?php echo $column_right; ?></div>
</div>
</div>
</div>


<script type="text/javascript"><!--
	$(document).on('change', 'input[name=\'account\']', function() {
		if ($('#collapse-payment-address').parent().find('.panel-heading .panel-title > *').is('a')) {
			if (this.value == 'register') {
				$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_account; ?> <i class="fa fa-caret-down"></i></a>');
			} else {
				$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');
			}
		} else {
			if (this.value == 'register') {
				$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_account; ?>');
			} else {
				$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_address; ?>');
			}
		}
	});

	<?php if (!$logged) { ?>
		$(document).ready(function() {
			$.ajax({
				url: 'index.php?route=checkout/login',
				dataType: 'html',
				success: function(html) {
					$('#collapse-checkout-option .panel-body').html(html);

					$('#collapse-checkout-option').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-option" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_option; ?> <i class="fa fa-caret-down"></i></a>');

					$('a[href=\'#collapse-checkout-option\']').trigger('click');
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});
	<?php } else { ?>
		$(document).ready(function() {
			$.ajax({
				url: 'index.php?route=checkout/shipping_address',
				dataType: 'html',
				success: function(html) {
				$('#collapse-shipping-address .panel-body').html(html);

				$('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

					$('a[href=\'#collapse-shipping-address\']').trigger('click');
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});
	<?php } ?>

	// Checkout
	$(document).delegate('#button-account', 'click', function() {
	window.location.assign("/index.php?route=account/register&back=1");
	});

	// Login
	$(document).delegate('#button-login', 'click', function() {
        console.log($('#logininput :input'));
		$.ajax({
			url: 'index.php?route=checkout/login/save',
			type: 'post',
			data: $('#logininput :input'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-login').button('loading');
			},
			complete: function() {
				$('#button-login').button('reset');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();
				$('.form-group').removeClass('has-error');

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					$('#collapse-checkout-option .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					// Highlight any found errors
					$('input[name=\'email\']').parent().addClass('has-error');
					$('input[name=\'password\']').parent().addClass('has-error');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	// Register
	$(document).delegate('#button-register', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/register/save',
			type: 'post',
			data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address textarea, #collapse-payment-address select'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-register').button('loading');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();
				$('.form-group').removeClass('has-error');

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					$('#button-register').button('reset');

					if (json['error']['warning']) {
						$('#collapse-payment-address .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

					for (i in json['error']) {
						var element = $('#input-payment-' + i.replace('_', '-'));

						if ($(element).parent().hasClass('input-group')) {
							$(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
						} else {
							$(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
						}
					}

					// Highlight any found errors
					$('.text-danger').parent().addClass('has-error');
				} else {
				<?php if ($shipping_required) { ?>
						var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').prop('value');

						if (shipping_address) {
							$.ajax({
								url: 'index.php?route=checkout/shipping_method',
								dataType: 'html',
								success: function(html) {
									// Add the shipping address
									$.ajax({
										url: 'index.php?route=checkout/shipping_address',
										dataType: 'html',
										success: function(html) {
											$('#collapse-shipping-address .panel-body').html(html);

											$('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');
										},
										error: function(xhr, ajaxOptions, thrownError) {
											alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
										}
									});

									$('#collapse-shipping-method .panel-body').html(html);

									$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

									$('a[href=\'#collapse-shipping-method\']').trigger('click');

									$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
									$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
									$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
								},
								error: function(xhr, ajaxOptions, thrownError) {
									alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
								}
							});
						} else {
							$.ajax({
								url: 'index.php?route=checkout/shipping_address',
								dataType: 'html',
								success: function(html) {
									$('#collapse-shipping-address .panel-body').html(html);

									$('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

									$('a[href=\'#collapse-shipping-address\']').trigger('click');

									$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
									$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
									$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
								},
								error: function(xhr, ajaxOptions, thrownError) {
									alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
								}
							});
						}
					<?php } else { ?>
						$.ajax({
							url: 'index.php?route=checkout/payment_method',
							dataType: 'html',
							success: function(html) {
								$('#collapse-payment-method .panel-body').html(html);

								$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

								$('a[href=\'#collapse-payment-method\']').trigger('click');

								$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					<?php } ?>

					$.ajax({
						url: 'index.php?route=checkout/payment_address',
						dataType: 'html',
						complete: function() {
							$('#button-register').button('reset');
						},
						success: function(html) {
							$('#collapse-payment-address .panel-body').html(html);

							$('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	// Payment Address
	$(document).delegate('#button-payment-address', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/payment_address/save',
			type: 'post',
			data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address textarea, #collapse-payment-address select'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-payment-address').button('loading');
			},
			complete: function() {
				$('#button-payment-address').button('reset');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					if (json['error']['warning']) {
						$('#collapse-payment-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

					for (i in json['error']) {
						var element = $('#input-payment-' + i.replace('_', '-'));

						if ($(element).parent().hasClass('input-group')) {
							$(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
						} else {
							$(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
						}
					}

					// Highlight any found errors
					$('.text-danger').parent().parent().addClass('has-error');
				} else {
				<?php if ($shipping_required) { ?>
						$.ajax({
							url: 'index.php?route=checkout/shipping_address',
							dataType: 'html',
							success: function(html) {
								$('#collapse-shipping-address .panel-body').html(html);

								$('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

								$('a[href=\'#collapse-shipping-address\']').trigger('click');

								$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
								$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
								$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					<?php } else { ?>
						$.ajax({
							url: 'index.php?route=checkout/payment_method',
							dataType: 'html',
							success: function(html) {
								$('#collapse-payment-method .panel-body').html(html);

								$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

								$('a[href=\'#collapse-payment-method\']').trigger('click');

								$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					<?php } ?>

					$.ajax({
						url: 'index.php?route=checkout/payment_address',
						dataType: 'html',
						success: function(html) {
							$('#collapse-payment-address .panel-body').html(html);
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	// Shipping Address
	$(document).delegate('#button-shipping-address', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/shipping_address/save',
			type: 'post',
			data: $('#chekingform input[type=\'text\'], #chekingform input[type=\'tel\'], #chekingform input[type=\'email\'], #chekingform input[type=\'date\'], #chekingform input[type=\'datetime-local\'], #chekingform input[type=\'time\'], #chekingform input[type=\'radio\']:checked, #chekingform textarea, #chekingform select'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-shipping-address').button('loading');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['error']) {
					$('#button-shipping-address').button('reset');

					if (json['error']['warning']) {
						$('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

					for (i in json['error']) {
						var element = $('#input-shipping-' + i.replace('_', '-'));

						if ($(element).parent().hasClass('input-group')) {
							$(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
						} else {
							$(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
						}
					}

					// Highlight any found errors
					$('.text-danger').parent().parent().addClass('has-error');
				} else {

					$.ajax({
						url: 'index.php?route=checkout/confirm',
						dataType: 'html',
						complete: function() {

						},
						success: function(html) {
							/*$('#collapse-checkout-confirm .panel-body').html(html);

							$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-confirm" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_confirm; ?> <i class="fa fa-caret-down"></i></a>');

							$('a[href=\'#collapse-checkout-confirm\']').trigger('click');

							document.getElementById('conf').style.display = 'block';*/
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});

					$.ajax({
						url: 'index.php?route=checkout/confirm',
						dataType: 'html',
						success: function(html) {
							var codelink = '<?php echo $codelink; ?>';
							window.location.assign(codelink);
							/*$('#button-shipping-address').button('reset');*/
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	// Guest
	$(document).delegate('#button-guest', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/guest/save',
			type: 'post',
			data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address textarea, #collapse-payment-address select'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-guest').button('loading');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					$('#button-guest').button('reset');

					if (json['error']['warning']) {
						$('#collapse-payment-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

					for (i in json['error']) {
						var element = $('#input-payment-' + i.replace('_', '-'));

						if ($(element).parent().hasClass('input-group')) {
							$(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
						} else {
							$(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
						}
					}

					// Highlight any found errors
					$('.text-danger').parent().addClass('has-error');
				} else {
				<?php if ($shipping_required) { ?>
						var shipping_address = $('#collapse-payment-address input[name=\'shipping_address\']:checked').prop('value');

						if (shipping_address) {
							$.ajax({
								url: 'index.php?route=checkout/shipping_method',
								dataType: 'html',
								complete: function() {
									$('#button-guest').button('reset');
								},
								success: function(html) {
									// Add the shipping address
									$.ajax({
										url: 'index.php?route=checkout/guest_shipping',
										dataType: 'html',
										success: function(html) {
											$('#collapse-shipping-address .panel-body').html(html);

											$('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');
										},
										error: function(xhr, ajaxOptions, thrownError) {
											alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
										}
									});

									$('#collapse-shipping-method .panel-body').html(html);

									$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

									$('a[href=\'#collapse-shipping-method\']').trigger('click');

									$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
									$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
								},
								error: function(xhr, ajaxOptions, thrownError) {
									alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
								}
							});
						} else {
							$.ajax({
								url: 'index.php?route=checkout/guest_shipping',
								dataType: 'html',
								complete: function() {
									$('#button-guest').button('reset');
								},
								success: function(html) {
									$('#collapse-shipping-address .panel-body').html(html);

									$('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

									$('a[href=\'#collapse-shipping-address\']').trigger('click');

									$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
									$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
									$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
								},
								error: function(xhr, ajaxOptions, thrownError) {
									alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
								}
							});
						}
					<?php } else { ?>
						$.ajax({
							url: 'index.php?route=checkout/payment_method',
							dataType: 'html',
							complete: function() {
								$('#button-guest').button('reset');
							},
							success: function(html) {
								$('#collapse-payment-method .panel-body').html(html);

								$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

								$('a[href=\'#collapse-payment-method\']').trigger('click');

								$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
							},
							error: function(xhr, ajaxOptions, thrownError) {
								alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
							}
						});
					<?php } ?>
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	// Guest Shipping
	$(document).delegate('#button-guest-shipping', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/guest_shipping/save',
			type: 'post',
			data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-guest-shipping').button('loading');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					$('#button-guest-shipping').button('reset');

					if (json['error']['warning']) {
						$('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

					for (i in json['error']) {
						var element = $('#input-shipping-' + i.replace('_', '-'));

						if ($(element).parent().hasClass('input-group')) {
							$(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
						} else {
							$(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
						}
					}

					// Highlight any found errors
					$('.text-danger').parent().addClass('has-error');
				} else {
					$.ajax({
						url: 'index.php?route=checkout/shipping_method',
						dataType: 'html',
						complete: function() {
							$('#button-guest-shipping').button('reset');
						},
						success: function(html) {
							$('#collapse-shipping-method .panel-body').html(html);

							$('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i>');

							$('a[href=\'#collapse-shipping-method\']').trigger('click');

							$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
							$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	$(document).delegate('#button-shipping-method', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/shipping_method/save',
			type: 'post',
			data: $('#collapse-shipping-method input[type=\'radio\']:checked, #collapse-shipping-method textarea'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-shipping-method').button('loading');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					$('#button-shipping-method').button('reset');

					if (json['error']['warning']) {
						$('#collapse-shipping-method .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}
				} else {
					$.ajax({
						url: 'index.php?route=checkout/payment_method',
						dataType: 'html',
						complete: function() {
							$('#button-shipping-method').button('reset');
						},
						success: function(html) {
							$('#collapse-payment-method .panel-body').html(html);

							$('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

							$('a[href=\'#collapse-payment-method\']').trigger('click');

							$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	$(document).delegate('#button-payment-method', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/payment_method/save',
			type: 'post',
			data: $('#collapse-payment-method input[type=\'radio\']:checked, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-payment-method').button('loading');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					if (json['error']['warning']) {
						$('#collapse-payment-method .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}
				} else {
					$.ajax({
						url: 'index.php?route=checkout/confirm',
						dataType: 'html',
						complete: function() {
							$('#button-payment-method').button('reset');
						},
						success: function(html) {
							$('#collapse-checkout-confirm .panel-body').html(html);

							$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-confirm" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_confirm; ?> <i class="fa fa-caret-down"></i></a>');

							$('a[href=\'#collapse-checkout-confirm\']').trigger('click');
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	$(document).delegate('#button-loginshov', 'click', function() {
		document.getElementById('login_dost').style.display = 'block';
	});



	//--></script>
	<script type="text/javascript"><!--
$('input[name=\'payment_address\']').on('change', function() {
	if (this.value == 'new') {
		$('#payment-existing').hide();
		$('#payment-new').show();
	} else {
		$('#payment-existing').show();
		$('#payment-new').hide();
	}
});
//--></script>
<script type="text/javascript"><!--
// Sort the custom fields
$('#collapse-payment-address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#collapse-payment-address .form-group').length) {
		$('#collapse-payment-address .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#collapse-payment-address .form-group').length) {
		$('#collapse-payment-address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#collapse-payment-address .form-group').length) {
		$('#collapse-payment-address .form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-payment-address button[id^=\'button-payment-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input[name^=\'custom_field\']').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-payment-address select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('#collapse-payment-address select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#collapse-payment-address input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('#collapse-payment-address input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('#collapse-payment-address select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#collapse-payment-address select[name=\'country_id\']').trigger('change');





	//--></script
//--></script>
		<script type="text/javascript">
				$(document).delegate('#button-coupon', 'click', function() {


					$.ajax({
						url: 'index.php?route=total/coupon/coupon',
						type: 'post',
						data: 'coupon=' + encodeURIComponent($('input[name=\'coupon\']').val()),
						dataType: 'json',
						beforeSend: function() {
							$('#button-coupon').button('loading');
						},
						complete: function() {
							$('#button-coupon').button('reset');
						},
						success: function(json) {
							$('.alert').remove();

							if (json['error']) {
								$('.trash-information').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');


							}

							if (json['redirect']) {
								location = json['redirect'];
							}
						}
					});
				});
		</script>
<?php echo $footer; ?>