<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<div id="review">
  <div class="row" id="review<?=$review['review_id']; ?>" style="    padding: 30px 0px;">
    <div class="col-sm-3">
      <img src="<?php echo $review['image']; ?>" class="kolo_2" alt="">
      <strong class="author_review"><?php echo $review['author']; ?></strong>
    </div>
    <div class="col-sm-9">
      <p class="text_review"><?php echo $review['text']; ?></p>
      <div class="row">
        <div class="col-sm-8 text-left">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($review['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x" style='color: #FC0;'></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x" style='color: #FC0;'></i><i class="fa fa-star-o fa-stack-2x" style='color: #E69500;'></i></span>
          <?php } ?>
          <?php } ?>
        </div>
        <div class="col-sm-4 text-right data_review"><?php echo $review['date_added']; ?></div>

      </div>
    </div>
  </div>
  <div class="text-right"></div>
</div>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 checkoutprodborder cart-aftr-line-style_KP" style="margin: 0 !important;"></div>
</div>
<?php } ?>

<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
