<?php echo $header; ?>
<div class="container">
  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 container_cart">
    <div class="cart_home_fix"> <?=$cart; ?></div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 no-padding">
    <div class="legend__inner">
      <span class="legend__left l-ab_villa leg_lef"></span>
                                <span class="legend__content about_villa-title" style="padding-left: 0">
    <ul class="breadcrumbmy breadcrumbmy-margin-style hidden-xs hidden-sm" style="padding-left: 0; margin-left: 33px">
      <?php
      $i = 1;
      $count = count($breadcrumbs);
      ?>
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if($count == $i){ ?>
      <li><a class="breadcrumbmy-last-elem"><?php echo $breadcrumb['text']; ?></a></li>
      <?php }else{ ?>
      <li><a style="font-weight: 100" class="breadcrumbmy-first-elem" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'].' - '; ?></a></li>
      <?php } ?>
      <?php $i+=1; ?>
      <?php } ?>
    </ul>
                                </span>
      <span class="legend__right l-ab_villa leg_rig_33 leg_lef"></span>
    </div>
    <!-- <ul class="nav nav-tab">
       <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
       <?php if ($review_status) { ?>
       <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
       <?php } ?>
       <li style="    border-right: 1px solid #ececec;"><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
     </ul> -->

    <div class=""><?php echo $column_left; ?>
      <?php if ($column_left && $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-9'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-12'; ?>
      <?php } ?>
      <div class="<?php echo $class; ?> content-top-div-style" style="padding-left: 0"><?php echo $content_top; ?>
        <?php if($product_type == 'service'){ ?>
        <h1 style="text-align:center;">Послуга</h1>
        <?php } ?>
        <div class="col-sm-12 co-back-2">
          <div class="">
            <div style="clear: both;"></div>
            <div class="">
              <div class="col-sm-2 hidden-lg hidden-md"></div>
              <div class="col-sm-12 col-lg-5 col-xl-4 col-md-5 col-xs-12 cl_poz_23">
                <ul class="thumbnails title_img_product">
                  <li><a class="thumbnailmy" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" style="position: relative; outline: none">
                      <?php if(($product_type == 'service') || ($product_type == 'productincart') || ($special)){ ?>
                      <div style="position: absolute;
                                    top: 23px;
                                    right: -42px;
                                    width: 170px;
                                    text-align: center;
                                    transform: rotate(39deg);
                                    font-family: 'Trebuchet MS';
                                    background-color: #ef7431;
                                    font-size: 14px;
                                    color: white;
                                    padding: 2px;">
                        <?php if($product_type == 'service'){ ?>
                        <b><?= $service; ?></b>
                        <?php }elseif(!($special)){ ?>
                        <b><?= $text_productincart; ?></b>
                        <?php }elseif($special){ ?>
                        <?php
                                $riznuca = ((float)$special/(float)$price) * 100;
                                $procent = 100-$riznuca;
                            ?>
                        <b>Акція - <?= round($procent); ?>%</b>
                        <?php } ?>
                      </div>
                      <?php } ?>
                      <img class="whi_img"  src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>"/>
                      <div class="row text-right no-margin im_left_pos">
                        <span class="pull-right"><img src="../image/zoom-in.png"></span>
                      </div>
                    </a>
                  </li>
                </ul>
              </div>
              <div class="col-sm-12 col-md-7 col-lg-7 col-xl-12 pading_product" id="product"style="padding-left: 0;">
                <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4 col-xs-12 fv">
                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
                        <span class="product_text_name_product mob_style" style="padding-left: 0 !important;"><?php echo $heading_title; ?></span>
                      </div>
                    </div>
                    <div class="hid_xs_bord_bot mob_text_20" style="text-align: justify;">
                      <p class="mob_text_20">
                        <?php echo $description; ?>
                      </p>
                    </div>
                  </div>
                  <!--   <div class="rating col-lg-4">
                       <?php for ($i = 1; $i <= 5; $i++) { ?>
                       <?php if ($rating < $i) { ?>
                       <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <?php } else { ?>
                       <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                       <?php } ?>
                       <?php } ?>
                       <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">(<?php echo $reviews; ?>)</a>
                     </div> -->
                </div>
                <div class="row">
                  <div class="col-sm-3 col-xs-3"></div>
                  <div class="col-sm-6 col-xs-6 col-md-12 col-lg-12 col-xl-4 product-text-style">
                    <?php if(($product_type != 'productincart') && ($product_type != 'service')){ ?>
                    <div class="row to_mar_left">
                      <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-6 product-description-style mob_text_20"><?php echo $text_weight; ?> </div>
                      <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-6 text-left product-description-style mob_text_20"><?php echo ($weight/1000).' '.$text_weight_kg; ?></div>
                    </div>
                    <div class="row to_mar_left">
                      <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-6 product-description-style mob_text_20"><?php echo $text_manufacturer; ?> </div>
                      <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-6 text-left product-description-style mob_text_20"><?php echo $manufacturer; ?> </div>
                    </div>
                    <div class="row to_mar_left">
                      <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-6 product-description-style mob_text_20"><?php echo $text_model; ?></div>
                      <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-6 text-left product-description-style mob_text_20"><?php echo $model; ?></div>
                    </div>
                    <div class="row to_mar_left">
                      <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6 col-xl-6 product-description-style mob_text_20"><?php echo $text_stock; ?></div>
                      <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-6 text-left product-description-style mob_text_20"><?php echo $stock; ?></div>
                    </div>
                    <?php } ?>
                    <?php if(($product_type == 'productincart') || ($product_type == 'service')){ ?>
                    <div class="row productincart-div-info">
                      <?php }else{ ?>
                      <div class="row" style="margin-top: 25px;">
                        <?php } ?>
                        <div class="col-sm-6 col-md-3 col-lg-3 col-xl-3 col-xs-6 mobile-input-div-margin no-padding">
                          <div class="row no-margin ro2_fl_left">
                            <img src="../image/fygura-47.png">
                            <div id="minus" class=" no-padding amount-icon-div mobile-input-icon-style">
                              <i  class="minus amount-icon-style">-</i>
                            </div>
                            <div class=" amount-input-div-style no-padding">
                              <input disabled="disabled" type="text" name="quantity" value="1" size="2" id="input-quantity"
                                     class="input_product text-center input-dimensions"/>
                            </div>
                            <div id="plus" class=" no-padding amount-icon-div2 mobile-input-icon-style">
                              <i  class="plus amount-icon-style">+</i>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 col-xs-6 prices-product-style22">
                          <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                          <input id="price" type="hidden" name="price" value="<?php echo (float)$price_only; ?>"/>
                          <?php $price_currency = (preg_replace ("/[^a-zA-ZА-Яа-я\s]/","",$price)); ?>
                          <?php $price_price = (preg_replace ("/[^0-9,\s]/","",$price)); ?>
                          <?php if (!$special) { ?>
                          <span class="mob_style"><span id="total_price" class="mob_style"><?php echo $price_price; ?></span><?php echo $carrency; ?></span>
                          <?php } else { ?>
                          <span class="special-price-style mob_style"><?php echo $price_currency; ?></span>
                          <span id="total_price" class="mob_style"><?php echo $special; ?></span>
                          <?php } ?>
                        </div>
                        <?php if(($product_type == 'productincart') || ($product_type == 'service')){ ?>
                        <div class="col-sm-12 col-md-5 col-lg-5 col-xl-12 col-xs-12 btn-cart-margin productincart-btn-div">
                          <?php }else{ ?>
                          <div class="col-sm-12 col-md-5 col-lg-5 col-xl-4 col-xs-12 btn-cart-margin" style="padding-left: 0px; margin-top: 5px;padding-right: 4px;">
                            <?php } ?>
                            <div class="product_<?=$product_id;?>">
                              <button type="button" id="button-cart" data-product="<?=$product_id;?>" class="button_style2 button_product button_style"><!--<i class="product-icon-position icon_corzinu"></i>--><span id="cart-btn-text" class="button_add_cart"><?php echo $button_cart; ?></span></button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php if ($products) { ?>
          <div style="clear: both"></div>
          <span class="legend__inner">
                                <span class="legend__left l-ab_villa hidden-xs"></span>
                                <span class="legend__content about_villa-title mob_style"><?php echo $text_related; ?></span>
                                <span class="legend__right l-ab_villa hidden-xs"></span>
	</span>
          <div class="KP-100" style="padding-left: 15px; margin-top: 15px;">
            <?php foreach ($products as $product) { ?>
            <div class="KP-20 col-sm-6 col-md-4 product_<?=$product['product_id'];?>">
              <div class="transition transition_KP" id="box_quantity_<?=$product['product_id']?>">
                <div class="image"><a href="<?php echo $product['href']; ?>">
                    <img src="<?php echo $product['thumb']; ?>"
                         alt="<?php echo $product['name']; ?>"
                         title="<?php echo $product['name']; ?>"
                         class="img-responsive"/></a>
                </div>
                <div class="text_featured_style">
                  <?php if ($product['special']) { ?>
                  <?php
					$riznuca = ($product['special']/$product['price']) * 100;
					$procent = 100-$riznuca;
				?>
                  <div style="position: absolute; top: 23px; right: -42px; width: 170px; text-align: center; transform: rotate(39deg); font-family: 'Trebuchet MS'; background-color: #ef7431;
									font-size: 14px;
									color: white;
									padding: 2px;"><b>Акція
                      - <?= round($procent); ?>%</b></div>
                  <?php } ?>
				<span><a class="text_featured mob_style"
                         href="<?php echo $product['href']; ?>"
                  ><?php echo $product['name']; ?></a></span>
                </div>
                <div class="mobile-input-div-margin">
                  <div class="row" style="margin-top: 18px">
                    <div class="col-xs-6">
                      <div class="counter-product counter-product_KP " style="">
                        <div class="no-padding amount-icon-div_KP mobile-input-icon-style">
                          <span class="minus amount-icon-style amount-icon-style-KP1" data-product="<?=$product['product_id']?>">-</span>
                        </div>
                        <div class="amount-input-div-style no-padding">
                          <input disabled="disabled" type="text" name="quantity" value="1" size="2"
                                 class="input_product text-center input-dimensions_KP input-quantity"/>
                        </div>
                        <div class=" no-padding amount-icon-div_KP mobile-input-icon-style">
                          <span class="plus amount-icon-style amount-icon-style-KP2 " data-product="<?=$product['product_id']?>">+</span>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-6" style="padding-left: 0;">
                      <?php if ($product['price']) { ?>
                      <div class="KP prices-product-style_KP">
                        <input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>"/>
                        <input class="price" type="hidden" name="price" value="<?php echo (float)$price_only; ?>"/>
                        <?php $price_currency = (preg_replace ("/[^a-zA-ZА-Яа-я\s]/","",$product['price'])); ?>
                        <?php $price_price = (preg_replace ("/[^0-9,\s]/","",$product['price'])); ?>
                        <?php $price_price = str_replace (",", ".", $price_price); ?>
                        <?php if (!$product['special']) { ?>
                        <span class="mob_style"><span class="total_price price_style mob_style"><?php echo $price_price.' '; ?></span><?php echo $carrency; ?></span>
                        <input type="hidden" class="price_val" value="<?php echo $price_price; ?>">
                        <?php } else { ?>
                        <span class="special-price-style mob_style"><?php echo $price_currency; ?></span>
                        <span class="total_price price-new_style mob_style"><?php echo $special; ?></span>
                        <?php } ?>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="button-group">
                  <button class="button_style" type="button"
                          onclick="cart.add('<?php echo $product['product_id']; ?>')">
						<span class="text_butoon_cart button_add_cart"
                        ><?php echo $button_cart; ?></span></button>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
          <?php } ?>
          <?php if ($tags) { ?>
          <p><?php echo $text_tags; ?>
            <?php for ($i = 0; $i < count($tags); $i++) { ?>
            <?php if ($i < (count($tags) - 1)) { ?>
            <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
            <?php } else { ?>
            <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
            <?php } ?>
            <?php } ?>
          </p>
          <?php } ?>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding text-center other-cpoduct-div">

          </div>
        </div>
        <?php if($product_type == 'productincart'){ ?>
        <?php if(isset($productincart)){ ?>
        <div style="padding-right: 15px; ">
          <table class='table' style="background-color: #fff; border-bottom: 1px solid #c8c8c8">
          <thead id="thead_click" class="thead-style">
          <tr>
            <td colspan="5" class="product-thead-title" style="padding-left: 30px !important;">
              <?=$entry_td_cart; ?>
            </td>
          </tr>
          </thead>
          <tbody id="tbody_click">
          <tr class="product-border-none-style">
            <td></td>
            <td></td>
            <td><?=$entry_td_name;?></td>
            <td><?=$entry_td_price;?></td>
            <td></td>
          </tr>
          <?php foreach($productincart as $key => $product_c){ ?>
          <tr class="product-border-none product-description-table-style">
            <td style="padding-top: 15px; padding-left: 30px; padding-right: 0">
              <?= $key+1; ?>
            </td>
            <td style="padding-top: 25px !important;">
              <img src="../image/<?=  $product_c['image_incart']; ?>" alt="" title="" style="width: 35px;height: 35px" />
            </td>
            <td style="padding-top: 17px;">
              <?=  $product_c['name']; ?>
            </td>
            <td style="padding-top: 17px;">
              <?=  $product_c['price']; ?>
            </td>
            <td style="padding-right: 30px !important; padding-top: 24px !important;">
              <a style="width: 100%" href="<?=  $product_c['href']; ?>" class='btn btn-success productincart-view-btn-style'><?=$entry_td_veiw; ?></a>
            </td>
          </tr>
          <?php } ?>
          </tbody>
        </table>
        </div>
        <?php } ?>
        <?php } ?>
        <div style="padding-right: 12px;">
          <?php echo $content_bottom; ?>
        </div>
      </div>
      <!--<nav  class="search_style" id="top_slyder">
        <h3 class="featured" style="text-align: center;     margin-top: 45px;"><?php echo $why_us; ?></h3>
        <div class="row search_container_style">
          <a href="">
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 container_style_icon" style="min-height: 110px;">
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 no_padding">
                <img class="img_home_style" src="../../../../../../image/catalog/demo/icon_actsii.png">
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text_home_style_2 no_padding" style="margin-top: 10px">
                <span class="text_home_style"><?php echo $more_shares; ?></span>
              </div>
            </div>
          </a>

          <a href="">
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 container_style_icon" style="min-height: 110px;">
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 no_padding">
                <img class="img_home_style_2" src="../../../../../../image/catalog/demo/icon_tovaru.png">
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 no_padding" style="    margin-top: 10px">
                <span class="text_home_style"><?php echo $the_best_range; ?></span>
              </div>
            </div>
          </a>

          <a href="">
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 container_style_icon_2" style="min-height: 110px;">
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 no_padding">
                <img class="img_home_style_3" src="../../../../../../image/catalog/demo/icon_03.png">
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="    margin-top: 10px">
                <span class="text_home_style"><?php echo $healthy_eating; ?></span>
              </div>
            </div>
          </a>

          <a href="">
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 container_style_icon_2" style="min-height: 110px;">
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 no_padding">
                <img class="img_home_style_4" src="../../../../../../image/catalog/demo/icon_04.png">
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 no_padding" style="    margin-top: 10px">
                <span class="text_home_style"><?php echo $loyalty_program; ?></span>
              </div>
            </div>
          </a>
        </div>
      </nav>-->
    </div>

  </div>
    <?php echo $footer; ?>
    <script type="text/javascript"><!--
      $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
        $.ajax({
          url: 'index.php?route=product/product/getRecurringDescription',
          type: 'post',
          data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
          dataType: 'json',
          beforeSend: function() {
            $('#recurring-description').html('');
          },
          success: function(json) {
            $('.alert, .text-danger').remove();

            if (json['success']) {
              $('#recurring-description').html(json['success']);
            }
          }
        });
      });
      //--></script>
    <script type="text/javascript"><!--
      $('#button-cart').on('click', function() {
        var product_id = $(this).attr('data-product');
        console.log(product_id);
        $.ajax({
          url: 'index.php?route=checkout/cart/add',
          type: 'post',
          data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
          dataType: 'json',
          beforeSend: function() {
//            $('#button-cart').button('loading');

          },
          complete: function() {
//            $('#button-cart').button('reset');
          },
          success: function(json) {
            console.log(json);
            $('.alert, .text-danger').remove();
            $('.form-group').removeClass('has-error');

            if (json['error']) {
              if (json['error']['option']) {
                for (i in json['error']['option']) {
                  var element = $('#input-option' + i.replace('_', '-'));

                  if (element.parent().hasClass('input-group')) {
                    element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                  } else {
                    element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                  }
                }
              }

              if (json['error']['recurring']) {
                $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
              }

              // Highlight any found errors
              $('.text-danger').parent().addClass('has-error');
            }

            if (json['success']) {
              $('#cart > ul').load('index.php?route=common/cart/info ul li');
              $('#blockcart').html('<span id="cart-total">' + json['total'] + '</span>');
              $('#blockcart').css('display', 'block');
//              var added_text = $("#text_btn-added").val();
//              $('#cart-btn-text').text(added_text);
              var added_text = $("#text_btn-added").val();
              $(".product_" + product_id + " #cart-btn-text").text(added_text);
              console.log(json['total']);



            }
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      });
      //--></script>
    <script type="text/javascript"><!--
      $('.date').datetimepicker({
        pickTime: false
      });

      $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
      });

      $('.time').datetimepicker({
        pickDate: false
      });

      $('button[id^=\'button-upload\']').on('click', function() {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
          clearInterval(timer);
        }

        timer = setInterval(function() {
          if ($('#form-upload input[name=\'file\']').val() != '') {
            clearInterval(timer);

            $.ajax({
              url: 'index.php?route=tool/upload',
              type: 'post',
              dataType: 'json',
              data: new FormData($('#form-upload')[0]),
              cache: false,
              contentType: false,
              processData: false,
              beforeSend: function() {
                $(node).button('loading');
              },
              complete: function() {
                $(node).button('reset');
              },
              success: function(json) {
                $('.text-danger').remove();

                if (json['error']) {
                  $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                }

                if (json['success']) {
                  alert(json['success']);

                  $(node).parent().find('input').attr('value', json['code']);
                }
              },
              error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
              }
            });
          }
        }, 500);
      });
      //--></script>
    <script type="text/javascript"><!--
      $('#review').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();

        $('#review').fadeOut('slow');

        $('#review').load(this.href);

        $('#review').fadeIn('slow');
      });

      $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

      $('#button-review').on('click', function() {
        $.ajax({
          url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
          type: 'post',
          dataType: 'json',
          data: $("#form-review").serialize(),
          beforeSend: function() {
            $('#button-review').button('loading');
          },
          complete: function() {
            $('#button-review').button('reset');
          },
          success: function(json) {
            $('.alert-success, .alert-danger').remove();

            if (json['error']) {
              $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
            }

            if (json['success']) {
              $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

              $('input[name=\'name\']').val('');
              $('textarea[name=\'text\']').val('');
              $('input[name=\'rating\']:checked').prop('checked', false);
            }
          }
        });
      });

      $(document).ready(function() {
        $('.thumbnails').magnificPopup({
          type:'image',
          delegate: 'a',
          gallery: {
            enabled:true
          }
        });
      });
      //--></script>
    <script type="text/javascript"><!--
      var price =  $('#price').val();
      jQuery(document).ready(function ($) {
        $('#minus').click(function () {
          document.getElementById('input-quantity').value--;
          var input = document.getElementById('input-quantity');
          var price_total = price;
          if (input.value > 0){
            price_total = input.value * price;
          }
          else if(input.value <= 1){
            input.value = 1;
            price_total = price;
          }
          $('#total_price').html(price_total.toFixed(2));
        });

        $('#plus').click(function () {
          document.getElementById('input-quantity').value++;
          var input = document.getElementById('input-quantity');
          var price_total = input.value * price;
          $('#total_price').html(price_total.toFixed(2));
        });
      });
      --></script>
    <script><!--
      $('#thead_click').on('click', function() {
        $('#i_class').toggleClass('fa-angle-up')
        $('#i_class').toggleClass('fa-angle-down')
        $('#tbody_click').toggle()
      })

      --></script>
    <script type="text/javascript"><!--
      var price =  $('.price').val();
      jQuery(document).ready(function ($) {
        $('.minus').click(function () {
          var product_id = $(this).data('product');
          var price =  $('#box_quantity_'+ product_id +' .price_val').val();
          console.log(price);
          var quantity =  $('#box_quantity_'+ product_id +' .input-quantity').val();
          var input = $('#box_quantity_'+ product_id +' .input-quantity');

          var price_total = price;
          if (input.val() > 1){
            input.val(quantity - 1);
            price_total = input.val() * price;
          }
          $('#box_quantity_'+ product_id +' .total_price').html(price_total.toFixed(2));
        });

        $('.plus').click(function () {
          var product_id = $(this).data('product');
          var price =  $('#box_quantity_'+ product_id +' .price_val').val();
          console.log(price);
          var quantity =  parseInt($('#box_quantity_'+ product_id +' .input-quantity').val());
          var input = $('#box_quantity_'+ product_id +' .input-quantity');
          input.val(quantity + 1);
          var price_total = input.val() * price;
          $('#box_quantity_'+ product_id +' .total_price').html(price_total.toFixed(2));
        });
      });
      --></script>
