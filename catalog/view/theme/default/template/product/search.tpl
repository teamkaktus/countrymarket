<?php echo $header; ?>
<div class="row no-margin">
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 container_cart">
            <div class="cart_home_fix"> <?=$cart; ?></div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 no-padding">
            <div class="">
                <!--<ul class="breadcrumb">
                  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                  <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                  <?php } ?>
                </ul> -->
                <div class=""><?php echo $column_left; ?>
                    <?php if ($column_left && $column_right) { ?>
                    <?php $class = 'col-sm-6'; ?>
                    <?php } elseif ($column_left || $column_right) { ?>
                    <?php $class = 'col-sm-9'; ?>
                    <?php } else { ?>
                    <?php $class = 'col-sm-12'; ?>
                    <?php } ?>
                    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                        <!-- <h1 class="text_title_search"><?php echo $heading_title; ?></h1>
                         <label class="control-label text_entry_search" for="input-search"><?php echo $entry_search; ?></label>-->
                        <div class="row search-filter-div-style">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 search-filter-padding">
                                <label class="control-label serch-title-filter-style" for="input-search"><?php echo $entry_search; ?></label>

                                <div class="pull-right hidden-xs hidden-sm hidden-md" style="overflow: hidden; height: 26px; margin-top: -3px;">
                                    <img src="../catalog/view/theme/default/image/rightBorder.png">
                                </div>
                                <!--<input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control input-sort_search" />-->
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-4 search-filter-padding" style="padding-right: 0; padding-left: 0">
                                <div class="row no-margin">
                                    <div class="col-xs-3 col-sm-5 col-md-5 col-lg-5" style="padding-right: 0">
                                        <label class="control-label serch-title-filter-style" for="input-sort"><?php echo $text_category; ?></label>
                                    </div>
                                    <div class="col-lg-7 col-sm-7 col-xs-5 col-md-7" style="padding-right: 0; padding-left: 0">
                                        <select id="input-sort" class="form-control search-input-sort_search-style no_padding" onchange="location = this.value;">
                                            <?php foreach ($categories as $category_1) { ?>
                                            <?php if ($category_1['category_id'] == $category_id) { ?>
                                            <option value="<?php echo $category_1['href']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $category_1['href']; ?>"><?php echo $category_1['name']; ?></option>
                                            <?php } ?>
                                            <?php foreach ($category_1['children'] as $category_2) { ?>
                                            <?php if ($category_2['category_id'] == $category_id) { ?>
                                            <option value="<?php echo $category_2['href']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $category_2['href']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
                                            <?php } ?>
                                            <?php foreach ($category_2['children'] as $category_3) { ?>
                                            <?php if ($category_3['category_id'] == $category_id) { ?>
                                            <option value="<?php echo $category_3['href']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $category_3['href']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-xs-5 col-sm-3 col-md-3 col-lg-3" style="padding-right: 0">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2" style="padding-right: 0; padding-left: 0">
                                    <label class="control-label serch-title-filter-style" for="input-sort"><?php echo $text_weight; ?></label>
                                </div>
                                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-8 text-center" style="margin-left:17px; ">
                                    <select id="input-sort" class="form-control search-input-sort_search-style no_padding" onchange="location = this.value;">
                                        <?php foreach ($weights as $weights) { ?>

                                        <?php if ($weights['weight_id'] == $weight) { ?>
                                        <option value="<?php echo $weights['href']; ?>"  selected="selected"><?php echo $weights['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $weights['href']; ?>"><?php echo $weights['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>
                            <div  class="col-xs-6 col-sm-5 col-md-4 col-lg-3">
                                <div class="row">
                                    <div class=" col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right" style="padding-right: 0" >
                                        <label class="control-label serch-title-filter-style" for="input-sort"><?php echo $text_price; ?></label>
                                    </div>
                                    <div class=" col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right">
                                        <select id="input-sort" style="padding-right: 18px !important;" class="form-control search-input-sort_search-style no_padding" onchange="location = this.value;">
                                            <?php foreach ($sorts as $sorts) { ?>
                                            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <!-- <p>
                             <label class="checkbox-inline text_description">
                                 <?php if ($description) { ?>
                                 <input type="checkbox" name="description" value="1" id="description" checked="checked" />
                                 <?php } else { ?>
                                 <input type="checkbox" name="description" value="1" id="description" />
                                 <?php } ?>
                                 <?php echo $entry_description; ?></label>
                         </p>
                         <div class="">
                             <label class="checkbox-inline text_description_2">
                                 <?php if ($sub_category) { ?>
                                 <input type="checkbox" name="sub_category" value="1" checked="checked" />
                                 <?php } else { ?>
                                 <input type="checkbox" name="sub_category" value="1" />
                                 <?php } ?>
                                 <?php echo $text_sub_category; ?></label>
                         </div>
                         <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary button_style_search" />
                         <h2 class="text_title_search"><?php echo $text_search; ?></h2>-->
                        <?php if ($products) { ?>
                        <!--<div class="row">
                          <div class="col-sm-3 hidden-xs">
                            <div class="btn-group">
                              <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
                              <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
                            </div>
                          </div>
                          <div class="col-sm-1 col-sm-offset-2 text-right">
                            <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                          </div>
                          <div class="col-sm-3 text-right">
                            <select id="input-sort" class="form-control col-sm-3" onchange="location = this.value;">
                              <?php foreach ($sorts as $sorts) { ?>
                              <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                              <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                              <?php } else { ?>
                              <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                              <?php } ?>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-sm-1 text-right">
                            <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                          </div>
                          <div class="col-sm-2 text-right">
                            <select id="input-limit" class="form-control" onchange="location = this.value;">
                              <?php foreach ($limits as $limits) { ?>
                              <?php if ($limits['value'] == $limit) { ?>
                              <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                              <?php } else { ?>
                              <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                              <?php } ?>
                              <?php } ?>
                            </select>
                          </div>
                        </div> -->
                        <br />
                        <div class="row" style="margin-bottom: 11px;">
                            <div class="KP-100" style=" margin-top: 15px;">
                                <?php foreach ($products as $product) { ?>
                                <div class="KP-20 col-sm-6 col-md-4 product_<?=$product['product_id'];?>">
                                    <div class="transition transition_KP" id="box_quantity_<?=$product['product_id']?>">
                                        <div class="image"><a href="<?php echo $product['href']; ?>">
                                                <img src="<?php echo $product['thumb']; ?>"
                                                     alt="<?php echo $product['name']; ?>"
                                                     title="<?php echo $product['name']; ?>"
                                                     class="img-responsive"/></a>
                                        </div>
                                        <div class="text_featured_style">
                                            <?php if ($product['special']) { ?>
                                            <?php
					$riznuca = ($product['special']/$product['price']) * 100;
					$procent = 100-$riznuca;
				?>
                                            <div style="position: absolute; top: 23px; right: -42px; width: 170px; text-align: center; transform: rotate(39deg); font-family: 'Trebuchet MS'; background-color: #ef7431;
									font-size: 14px;
									color: white;
									padding: 2px;"><b>Акція
                                                    - <?= round($procent); ?>%</b></div>
                                            <?php } ?>
				<span><a class="text_featured mob_style"
                         href="<?php echo $product['href']; ?>"
                    ><?php echo $product['name']; ?></a></span>
                                        </div>
                                        <div class="mobile-input-div-margin">
                                            <div class="row" style="margin-top: 18px">
                                                <div class="col-xs-6">
                                                    <div class="counter-product counter-product_KP " style="">
                                                        <div class="no-padding amount-icon-div_KP mobile-input-icon-style">
                                                            <span class="minus amount-icon-style amount-icon-style-KP1 mob_style" data-product="<?=$product['product_id']?>">-</span>
                                                        </div>
                                                        <div class="amount-input-div-style no-padding">
                                                            <input disabled="disabled" type="text" name="quantity" value="1" size="2"
                                                                   class="input_product text-center input-dimensions_KP input-quantity mob_style"/>
                                                        </div>
                                                        <div class=" no-padding amount-icon-div_KP mobile-input-icon-style">
                                                            <span class="plus amount-icon-style amount-icon-style-KP2 mob_style" data-product="<?=$product['product_id']?>">+</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <?php if ($product['price']) { ?>
                                                    <div class="KP prices-product-style_KP">
                                                        <input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>"/>
                                                        <input class="price" type="hidden" name="price" value="<?php echo (float)$price_only; ?>"/>
                                                        <?php $price_currency = (preg_replace ("/[^a-zA-ZА-Яа-я\s]/","",$product['price'])); ?>
                                                        <?php $price_price = (preg_replace ("/[^0-9,\s]/","",$product['price'])); ?>
                                                        <?php $price_price = str_replace (",", ".", $price_price); ?>
                                                        <?php if (!$product['special']) { ?>
                                                        <span class="mob_style"><span class="total_price price_style mob_style"><?php echo $price_price; ?></span><?=$carrency;?></span>
                                                        <input type="hidden" class="price_val" value="<?php echo $price_price; ?>">
                                                        <?php } else { ?>
                                                        <span class="special-price-style mob_style"><?php echo $price_currency; ?></span>
                                                        <span class="total_price price-new_style mob_style"><?php echo $special; ?></span>
                                                        <?php } ?>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="button-group">
                                            <button class="button_style" type="button"
                                                    onclick="cart.add('<?php echo $product['product_id']; ?>')">
                                                <span class="text_butoon_cart button_add_cart"><?php echo $button_cart; ?></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <section class="pagination">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-24">
                                        <div class="pagination_wrap">
                                            <?php echo $pagination; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <?php } else { ?>
                        <div class="text-center" style="margin-top: 25px">
                            <span class="legend__content "><?php echo $text_empty; ?></span>
                        </div>
                        <?php } ?>


                        <?php echo $content_bottom; ?></div>
                    <?php echo $column_right; ?></div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript"><!--
    $('#button-search').bind('click', function() {
        url = 'index.php?route=product/search';

        var search = $('#content input[name=\'search\']').prop('value');

        if (search) {
            url += '&search=' + encodeURIComponent(search);
        }

        var category_id = $('#content select[name=\'category_id\']').prop('value');

        if (category_id > 0) {
            url += '&category_id=' + encodeURIComponent(category_id);
        }

        var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

        if (sub_category) {
            url += '&sub_category=true';
        }

        var filter_description = $('#content input[name=\'description\']:checked').prop('value');

        if (filter_description) {
            url += '&description=true';
        }

        location = url;
    });

    $('#content input[name=\'search\']').bind('keydown', function(e) {
        if (e.keyCode == 13) {
            $('#button-search').trigger('click');
        }
    });

    $('select[name=\'category_id\']').on('change', function() {
        if (this.value == '0') {
            $('input[name=\'sub_category\']').prop('disabled', true);
        } else {
            $('input[name=\'sub_category\']').prop('disabled', false);
        }
    });

    $('select[name=\'category_id\']').trigger('change');
    --></script>
<script type="text/javascript"><!--
    var price =  $('.price').val();
    jQuery(document).ready(function ($) {
        $('.minus').click(function () {
            var product_id = $(this).data('product');
            var price =  $('#box_quantity_'+ product_id +' .price_val').val();
            console.log(price);
            var quantity =  $('#box_quantity_'+ product_id +' .input-quantity').val();
            var input = $('#box_quantity_'+ product_id +' .input-quantity');

            var price_total = price;
            if (input.val() > 1){
                input.val(quantity - 1);
                price_total = input.val() * price;
            }
            $('#box_quantity_'+ product_id +' .total_price').html(price_total.toFixed(2));
        });

        $('.plus').click(function () {
            var product_id = $(this).data('product');
            var price =  $('#box_quantity_'+ product_id +' .price_val').val();
            console.log(price);
            var quantity =  parseInt($('#box_quantity_'+ product_id +' .input-quantity').val());
            var input = $('#box_quantity_'+ product_id +' .input-quantity');
            input.val(quantity + 1);
            var price_total = input.val() * price;
            $('#box_quantity_'+ product_id +' .total_price').html(price_total.toFixed(2));
        });
    });
    --></script>

<?php echo $footer; ?>
