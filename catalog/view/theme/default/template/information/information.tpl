<?php echo $header; ?>
<div class="container">
    <div class="legend__inner hidden-xs">
                                <span class="legend__information about_villa-title">
                                  <ul class="breadcrumbmy breadcrumbmy_KP breadcrumbmy-margin-style">
                                      <?php
      $i = 1;
      $count = count($breadcrumbs);
    ?>
                                      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                                      <?php if($count == $i){ ?>
                                      <li><a class="breadcrumbmy-last-elem"><?php echo $breadcrumb['text']; ?></a></li>
                                      <?php }else{ ?>
                                      <li><a class="breadcrumbmy-first-elem"
                                             href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'].' - '; ?></a>
                                      </li>
                                      <?php } ?>
                                      <?php $i+=1; ?>
                                      <?php } ?>
                                  </ul>
                                </span>
        <span class="legend__right-information l-ab_villa" style="width: 100%"></span>
    </div>

    <div class=""><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content-information-KP" class="panel panel-default"><?php echo $content_top; ?>
            <div class="cart-part1-contact-style_KP"><span
                        class="cart-part1-h4-style"><?php echo $heading_title; ?></span></div>
            <div style="margin-top: 25px; padding: 20px"><?php echo $description; ?></div>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>