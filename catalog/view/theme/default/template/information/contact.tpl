<?php echo $header; ?>
<div class="container">
  <div class="legend__inner hidden-xs">
                                <span class="legend__information about_villa-title">
                                  <ul class="breadcrumbmy breadcrumbmy_KP breadcrumbmy-margin-style">
                                    <?php
      $i = 1;
      $count = count($breadcrumbs);
    ?>
                                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                                    <?php if($count == $i){ ?>
                                    <li><a class="breadcrumbmy-last-elem"><?php echo $breadcrumb['text']; ?></a></li>
                                    <?php }else{ ?>
                                    <li><a class="breadcrumbmy-first-elem"
                                           href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'].' - '; ?></a>
                                    </li>
                                    <?php } ?>
                                    <?php $i+=1; ?>
                                    <?php } ?>
                                  </ul>
                                </span>
    <span class="legend__right-information l-ab_villa" style="width: 100%"></span>
  </div>
  <div class=""><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div class="panel panel-default content-information-contact-KP"><?php echo $content_top; ?>
      <div class="cart-part1-div-style_KP"><span class="cart-part1-h4-style"><?php echo $heading_title; ?></span></div>
      <div style="padding: 0 20px">
        <h3 class="text_location_contact"><?php echo $text_location; ?></h3>
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <?php if ($image) { ?>
              <div class="col-sm-3"><img src="<?php echo $image; ?>" alt="<?php echo $store; ?>" title="<?php echo $store; ?>" class="img-thumbnail" /></div>
              <?php } ?>
              <div class="col-sm-3"><strong class="store_contact"><?php echo $store; ?></strong><br />
                <address class="address_contact">
                  <?php echo $address; ?>
                </address>
                <?php if ($geocode) { ?>
                <a href="https://maps.google.com/maps?q=<?php echo urlencode($geocode); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> <?php echo $button_map; ?></a>
                <?php } ?>
              </div>
              <div class="col-sm-3"><strong class="text_telephone_contact"><?php echo $text_telephone; ?></strong><br>
                <div class="telephone_contact"><?php echo $telephone; ?><br /></div>
                <br />
                <?php if ($fax) { ?>
                <strong class="text_fax_contact"><?php echo $text_fax; ?></strong><br>
                <div class="fax_contact"><?php echo $fax; ?></div>
                <?php } ?>
              </div>
              <div class="col-sm-3">
                <?php if ($open) { ?>
                <strong><?php echo $text_open; ?></strong><br />
                <?php echo $open; ?><br />
                <br />
                <?php } ?>
                <?php if ($comment) { ?>
                <strong><?php echo $text_comment; ?></strong><br />
                <?php echo $comment; ?>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <?php if ($locations) { ?>
        <h3><?php echo $text_store; ?></h3>
        <div class="panel-group" id="accordion">
          <?php foreach ($locations as $location) { ?>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"><a href="#collapse-location<?php echo $location['location_id']; ?>" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><?php echo $location['name']; ?> <i class="fa fa-caret-down"></i></a></h4>
            </div>
            <div class="panel-collapse collapse" id="collapse-location<?php echo $location['location_id']; ?>">
              <div class="panel-body">
                <div class="row">
                  <?php if ($location['image']) { ?>
                  <div class="col-sm-3"><img src="<?php echo $location['image']; ?>" alt="<?php echo $location['name']; ?>" title="<?php echo $location['name']; ?>" class="img-thumbnail" /></div>
                  <?php } ?>
                  <div class="col-sm-3"><strong><?php echo $location['name']; ?></strong><br />
                    <address>
                      <?php echo $location['address']; ?>
                    </address>
                    <?php if ($location['geocode']) { ?>
                    <a href="https://maps.google.com/maps?q=<?php echo urlencode($location['geocode']); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> <?php echo $button_map; ?></a>
                    <?php } ?>
                  </div>
                  <div class="col-sm-3"> <strong><?php echo $text_telephone; ?></strong><br>
                    <?php echo $location['telephone']; ?><br />
                    <br />
                    <?php if ($location['fax']) { ?>
                    <strong><?php echo $text_fax; ?></strong><br>
                    <?php echo $location['fax']; ?>
                    <?php } ?>
                  </div>
                  <div class="col-sm-3">
                    <?php if ($location['open']) { ?>
                    <strong><?php echo $text_open; ?></strong><br />
                    <?php echo $location['open']; ?><br />
                    <br />
                    <?php } ?>
                    <?php if ($location['comment']) { ?>
                    <strong><?php echo $text_comment; ?></strong><br />
                    <?php echo $location['comment']; ?>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
        <?php } ?>

      </div>

    </div>

      <?php echo $content_bottom; ?>
<div id="content-information-contact-KP">
  <form  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
    <fieldset class="panel fieldset_KP">
      <div class="cart-part1-contact-style_KP"> <span class="cart-part1-h4-style"><?php echo $text_contact; ?></span></div>
      <div class="form-group required required_KP">
        <label class="col-sm-2 control-label entry_name_contact" for="input-name"><?php echo $entry_name; ?></label>
        <div class="col-sm-10">
          <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control input-email_KP" />
          <?php if ($error_name) { ?>
          <div class="text-danger error_name_contact"><?php echo $error_name; ?></div>
          <?php } ?>
        </div>
      </div>
      <div class="form-group required required_KP-2">
        <label class="col-sm-2 control-label entry_email_contact" for="input-email"><?php echo $entry_email; ?></label>
        <div class="col-sm-10">
          <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control input-email_KP" />
          <?php if ($error_email) { ?>
          <div class="text-danger error_email_contact"><?php echo $error_email; ?></div>
          <?php } ?>
        </div>
      </div>
      <div class="form-group required required_KP-2">
        <label class="col-sm-2 control-label entry_enquiry_contact" for="input-enquiry"><?php echo $entry_enquiry; ?></label>
        <div class="col-sm-10">
          <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control input-email_KP"><?php echo $enquiry; ?></textarea>
          <?php if ($error_enquiry) { ?>
          <div class="text-danger error_enquiry_contact"><?php echo $error_enquiry; ?></div>
          <?php } ?>
        </div>
      </div>
      <?php echo $captcha; ?>
      <div class="buttons" style="margin-top: 20px">
        <div class="pull-right">
          <input class="btn btn_save_KP" style="margin: 0 20px 20px 0;" type="submit" value="<?php echo $button_submit; ?>" />
        </div>
      </div>
    </fieldset>
  </form>
</div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
