<div class="row ">
	<span class="legend__inner">
                                <span class="legend__left l-ab_villa"></span>
                                <span class="legend__content about_villa-title mob_style"><?php echo $heading_title; ?></span>
                                <span class="legend__right l-ab_villa"></span>
	</span>
	<div class="KP-100" style="padding-left: 15px; margin-top: 15px;">
		<?php foreach ($products as $product) { ?>
		<div class="KP-20 col-sm-6 col-md-4 product_<?=$product['product_id'];?>">
			<div class="transition transition_KP" id="box_quantity_<?=$product['product_id']?>">
				<div class="image"><a href="<?php echo $product['href']; ?>">
					<img  src="<?php echo $product['thumb']; ?>"
						alt="<?php echo $product['name']; ?>"
						title="<?php echo $product['name']; ?>"
						class="img-responsive"/></a>
				</div>
				<div class="text_featured_style">
					<?php if ($product['special']) { ?>
					<?php
					$riznuca = ($product['special']/$product['price']) * 100;
					$procent = 100-$riznuca;
				?>
					<div style="position: absolute; top: 23px; right: -42px; width: 170px; text-align: center; transform: rotate(39deg); font-family: 'Trebuchet MS'; background-color: #ef7431;
									font-size: 14px;
									color: white;
									padding: 2px;"><b>Акція
							- <?= round($procent); ?>%</b></div>
					<?php } ?>
				<span><a class="text_featured mob_style"
						 href="<?php echo $product['href']; ?>"
						><?php echo $product['name']; ?></a></span>
				</div>
				<div class="mobile-input-div-margin">
					<div class="row" style="margin-top: 18px">
						<div class="col-xs-6">
							<div class="counter-product counter-product_KP " style="">
								<div class="no-padding amount-icon-div_KP mobile-input-icon-style">
									<span class="minus amount-icon-style amount-icon-style-KP1 mob_style" data-product="<?=$product['product_id']?>">-</span>
								</div>
								<div class="amount-input-div-style no-padding">
									<input disabled="disabled" type="text" name="quantity" value="1" size="2"
										class="input_product text-center input-dimensions_KP input-quantity mob_style"/>
								</div>
								<div class=" no-padding amount-icon-div_KP mobile-input-icon-style">
									<span class="plus amount-icon-style amount-icon-style-KP2 mob_style" data-product="<?=$product['product_id']?>">+</span>
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<?php if ($product['price']) { ?>
							<div class="KP prices-product-style_KP">
								<input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>"/>
								<input class="price mob_style" type="hidden" name="price" value="<?php echo (float)$price_only; ?>"/>
								<?php $price_currency = (preg_replace ("/[^a-zA-ZА-Яа-я\s]/","",$product['price'])); ?>
								<?php $price_price = (preg_replace ("/[^0-9,\s]/","",$product['price'])); ?>
								<?php $price_price = str_replace (",", ".", $price_price); ?>
								<?php if (!$product['special']) { ?>
								<span><span class="total_price price_style mob_style"><?php echo $price_price; ?></span><?php echo $carrency; ?></span>
								<input type="hidden" class="price_val mob_style" value="<?php echo $price_price; ?>">
								<?php } else { ?>
								<span class="special-price-style mob_style"><?php echo $price_currency; ?></span>
								<span class="total_price price-new_style mob_style"><?php echo $special; ?></span>
								<?php } ?>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="button-group">
					<button class="button_style" type="button"
							onclick="cart.add('<?php echo $product['product_id']; ?>')">
						<span class="text_butoon_cart button_add_cart"
						><?php echo $button_cart; ?></span></button>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
<script type="text/javascript"><!--
	var price =  $('.price').val();
	jQuery(document).ready(function ($) {
		$('.minus').click(function () {
			var product_id = $(this).data('product');
			var price =  $('#box_quantity_'+ product_id +' .price_val').val();
			console.log(price);
			var quantity =  $('#box_quantity_'+ product_id +' .input-quantity').val();
			var input = $('#box_quantity_'+ product_id +' .input-quantity');

			var price_total = price;
			if (input.val() > 1){
				input.val(quantity - 1);
				price_total = input.val() * price;
			}
			$('#box_quantity_'+ product_id +' .total_price').html(price_total.toFixed(2));
		});

		$('.plus').click(function () {
			var product_id = $(this).data('product');
			var price =  $('#box_quantity_'+ product_id +' .price_val').val();
			console.log(price);
			var quantity =  parseInt($('#box_quantity_'+ product_id +' .input-quantity').val());
			var input = $('#box_quantity_'+ product_id +' .input-quantity');
			input.val(quantity + 1);
			var price_total = input.val() * price;
			$('#box_quantity_'+ product_id +' .total_price').html(price_total.toFixed(2));
		});
	});
	--></script>

