<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 slideshow_container">
    <div id="slideshow<?php echo $module; ?>" class="owl-carousel owl-theme">
        <?php foreach ($banners as $banner) { ?>
        <div class="item">
            <?php if ($banner['link']) { ?>
            <a href="<?php echo $banner['link']; ?>"><img class="image_banner" src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a>
            <?php } else { ?>
            <img class="image_banner" src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>"/>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
</div>
<script type="text/javascript">
$('#slideshow<?php echo $module; ?>').owlCarousel({
                items: 1,
                loop: true,
                nav: true,
                navClass:['q','w'],
                navText: ['<img src="../../../image/catalog/demo/Strylka_vlivo.png" />', '<img src="../../../image/catalog/demo/Strilka_vpravo.png" />'],
                singleItem: true,
                dots: true,
                margin: 0,
                dotsClass:'slider_pager'
    })
</script>