<span class="legend__inner3">
                                <span class="legend__left3 l-ab_villa hidden-xs"></span>
                                <span class="legend__content3 about_villa-title mob_style"><?php echo $heading_title; ?></span>
                                <span class="legend__right3 l-ab_villa hidden-xs"></span>
	</span>
<div class="row" style="margin-top: 23px; margin-bottom: 23px;" >
	<?php foreach ($products as $product) { ?>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 featured2_KP" >
		<div class="product-day product_<?=$product['product_id'];?>">
			<div class="row">
				<div class="col-lg-4 col-md-5 col-sm-5 col-xs-5">
					<a href="<?php echo $product['href']; ?>">
						<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
					</a>
				</div>
				<div class="caption col-lg-8 col-md-7 col-sm-7 col-xs-7">
					<h4 class="featured2_text mob_style">
						<a class="mob_style" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
					</h4>
					<span class="featured2_text_p"><?php echo $product['description']; ?></span>
					<div class="row" style="margin-top: 34px">
						<div class="col-sm-4">
							<?php if ($product['price']) { ?>
							<p class="featured2_text_p_price mob_style">
								<?php if (!$product['special']) { ?>
								<?php echo $product['price']; ?>
								<?php } else { ?>
								<span class="price-new mob_style">
									<?php echo $product['special']; ?>
								</span>
								<span class="price-old mob_style">
									<?php echo $product['price']; ?>
								</span>
								<?php } ?>
								<?php if ($product['tax']) { ?>
								<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
								<?php } ?>
							</p>
							<?php } ?>
						</div>
						<div class="col-sm-8">
							<div class="button-group KP_button-group">
								<button class="button_style_featured2 mob_style mob_button_style" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');">
									<span class="text_butoon_cart button_add_cart ">
										<?php echo $button_cart; ?>
									</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="block_style block_style_KP_2 col-xs-12 col-sm-12 col-md-12 col-lg-4">
		<div class="style_viget">
			<div class="fb-page" data-href="https://www.facebook.com/roketdevteam/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/roketdevteam/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/roketdevteam/">RoketDev Web &amp; Mobile</a></blockquote></div>
		</div>
	</div>
	<?php } ?>
</div>
