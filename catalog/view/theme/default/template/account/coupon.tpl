<?php echo $header; ?>
<div class="container">
    <div class="legend__inner hidden-xs">
                                <span class="legend__information about_villa-title">
                                  <ul class="breadcrumbmy breadcrumbmy_KP breadcrumbmy-margin-style">
                                      <?php
      $i = 1;
      $count = count($breadcrumbs);
    ?>
                                      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                                      <?php if($count == $i){ ?>
                                      <li><a class="breadcrumbmy-last-elem"><?php echo $breadcrumb['text']; ?></a></li>
                                      <?php }else{ ?>
                                      <li><a class="breadcrumbmy-first-elem"
                                             href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'].' - '; ?></a>
                                      </li>
                                      <?php } ?>
                                      <?php $i+=1; ?>
                                      <?php } ?>
                                  </ul>
                                </span>
        <span class="legend__right-information l-ab_villa" style="width: 100%"></span>
    </div>
  <div class=""><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8 col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
      <div id="content-order-KP" class="panel panel-default"><?php echo $content_top; ?>
      <div class="prof">
        <ul class="nav nav-tabs cart-part1-div-style_order_KP">
            <li role="presentation" >
                <a class="cart-part1-h4-style" style="color: #333" href="../../../index.php?route=account/edit"><?php echo $heading_title1; ?></a>
            </li>
            <li role="presentation">
                <a class="cart-part1-h4-style" style="color: #333" href="../../../index.php?route=account/order"><?php echo $heading_zam; ?></a>
            </li>
            <li role="presentation">
                <a class="cart-part1-h4-style" style="color: #333" href="/index.php?route=account/notification"><?php echo $heading_spov; ?>
                  <?php if($notifications_count) { ?>
                  <div class="notif">
                    <?=$notifications_count?>
                  </div>
                  <?php } ?>
                </a>
            </li>
            <li role="presentation" class="active">
                <a class="cart-part1-h4-style" style="color: #333" href="/index.php?route=account/coupon"><?php echo $heading_kyp; ?></a>
            </li>
        </ul>
      <div class="coupon-row">
        <?php foreach($coupons as $coupon) { ?>
          <?php
          $today = getdate();
          $date_end = strtotime($coupon['date_end']);
          $date_start = strtotime($coupon['date_start']);
          ?>

        <?php if(($date_start < $today[0] )&&($date_end > $today[0] )){ ?>
            <div class="col-xs-12" style="margin-top: 25px">
        <?php  }else{ ?>
            <div class="col-xs-12" style="color: rgba(125, 125, 125, 0.5);">
        <?php  } ?>

          <div class="col-xs-2 text_empty_style_order">
            <?=$coupon['name']?>
          </div>
                <div class="col-xs-3 text_empty_style_order">
                    <?
                        switch ($coupon['type']) {
                             case 'P':
                                 echo "Знижка на доставку";
                             break;
                             case 'F':
                                 echo "Знижка на товар";
                             break;
                             case 'K':
                                  echo "Знижка на комісію";
                             break;
                                    }
                    ?>
                </div>
          <div class="col-xs-3 text_empty_style_order">
            <?=$coupon['code']?>
          </div>
          <div class="col-xs-4 text_empty_style_order">
            <?php if($coupon['shipping']) { ?>
            <p>Безкоштвна доставка</p>
            <?php } ?>
            <?php if($coupon['discount']>0) { ?>
            <p>
              <?=$coupon['discount']?>
              <?php if($coupon['type']=='P') { ?>
                &#37
              <?php } else{ ?>
                грн.
              <?php } ?>
            </p>
            <?php } ?>
          </div>
        </div>
        <hr>
        <?php } ?>
      <?php echo $content_bottom; ?>
      </div>
    <?php echo $column_right; ?>
    <div class="clearfix"></div>
    </div>
</div>
</div>
</div>
<?php echo $footer; ?>
