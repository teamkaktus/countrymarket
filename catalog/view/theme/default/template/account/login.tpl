<?php echo $header; ?>
<div class="container">
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class=""><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8 col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>

    <div class="legend__inner hidden-xs">
                                <span class="legend__information about_villa-title">
                                  <ul class="breadcrumbmy breadcrumbmy_KP breadcrumbmy-margin-style">
                                    <?php
      $i = 1;
      $count = count($breadcrumbs);
    ?>
                                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                                    <?php if($count == $i){ ?>
                                    <li><a class="breadcrumbmy-last-elem"><?php echo $breadcrumb['text']; ?></a></li>
                                    <?php }else{ ?>
                                    <li><a class="breadcrumbmy-first-elem"
                                           href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'].' - '; ?></a>
                                    </li>
                                    <?php } ?>
                                    <?php $i+=1; ?>
                                    <?php } ?>
                                  </ul>
                                </span>
      <span class="legend__right-information l-ab_villa" style="width: 100%"></span>
    </div>
    <div id="content-KP" class="panel panel-default"><?php echo $content_top; ?>
      <div class="cart-part1-div-style_KP">
        <h4 class="cart-part1-h4-style"><?php echo $text_login; ?></h4>
      </div>
        <div class="col-sm-6 col-lg-6">
          <div class="checkoutpadding_KP">
            <h2 class="text-center cart-new-customer-style_KP"><?php echo $text_new_customer; ?></h2>
            <div class="radio text-left cart-text_register_checkout"><label style="padding-left: 0; font-size: 14px;"><?php echo $text_register; ?></label></div>
            <p><?php echo $text_register_account; ?></p>
            <div class="container_button_continue_KP">
              <a href="<?php echo $register; ?>" class="btn button_cart button_cart_KP "><?php echo $button_continue; ?></a></div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-6">
          <div class="checkoutpadding_KP-2">
            <h2 class="text-center cart-new-customer-style_KP"><?php echo $text_returning_customer; ?></h2>
            <div class="radio text-left cart-text_register_checkout"><label style="padding-left: 0; font-size: 14px;"><?php echo $text_i_am_returning_customer; ?></label></div>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
              <div class="form-group form-group_KP">
                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control input-email_KP" />
              </div>
              <div class="form-group form-group_KP-2">
                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control input-email_KP" />
              </div>
              <div class="hidden-md hidden-lg col-xs-12 col-sm-12 text-left no-padding" style="margin-bottom: 20px;">
                <a class="text_forgotten" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
              </div>
              <div class="col-md-6 col-lg-8 no-padding button_login_style_KP">
                <input type="submit" value="<?php echo $button_login; ?>"
                       class="btn button_cart button_cart_KP "/>
              </div>
              <div class="hidden-xs hidden-sm col-md-6 col-lg-4 text-right no-padding" style="margin-top: 10px;">
                <a class="text_forgotten" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
              </div>
              <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
<?php echo $footer; ?>