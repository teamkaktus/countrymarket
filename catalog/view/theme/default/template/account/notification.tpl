<?php echo $header; ?>
<div class="container">
    <div class="legend__inner hidden-xs">
                                <span class="legend__information about_villa-title">
                                  <ul class="breadcrumbmy breadcrumbmy_KP breadcrumbmy-margin-style">
                                      <?php
      $i = 1;
      $count = count($breadcrumbs);
    ?>
                                      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                                      <?php if($count == $i){ ?>
                                      <li><a class="breadcrumbmy-last-elem"><?php echo $breadcrumb['text']; ?></a></li>
                                      <?php }else{ ?>
                                      <li><a class="breadcrumbmy-first-elem"
                                             href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'].' - '; ?></a>
                                      </li>
                                      <?php } ?>
                                      <?php $i+=1; ?>
                                      <?php } ?>
                                  </ul>
                                </span>
        <span class="legend__right-information l-ab_villa" style="width: 100%"></span>
    </div>
  <div class=""><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8 col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
      <div id="content-order-KP" class="panel panel-default"><?php echo $content_top; ?>
      <div class="prof">
        <ul class="nav nav-tabs cart-part1-div-style_order_KP">
            <li role="presentation" >
                <a class="cart-part1-h4-style" style="color: #333" href="../../../index.php?route=account/edit"><?php echo $heading_title1; ?></a>
            </li>
            <li role="presentation">
                <a class="cart-part1-h4-style" style="color: #333"  href="../../../index.php?route=account/order"><?php echo $heading_zam; ?></a>
            </li>
            <li role="presentation" class="active">
                <a class="cart-part1-h4-style" style="color: #333" href="/index.php?route=account/notification"><?php echo $heading_spov; ?>
              <!--    <?php if($count) { ?>
                  <div class="notif">
                    <?=$count?>
                  </div>
                  <?php } ?> -->
                </a>
            </li>
            <li role="presentation" >
                <a class="cart-part1-h4-style" style="color: #333" href="/index.php?route=account/coupon"><?php echo $heading_kyp; ?></a>
            </li>
        </ul>
      <div class="col-sm-12">
          <?php if(!($notifications)){ ?>
          <div>
              <div class="col-sm-12" style="margin-top: 30px">
                  <span class="text_empty_style_order"><?php echo $text_empty_not; ?></span>
              </div>
          </div>
          <?php } ?>
        <?php foreach($notifications as $notification) { ?>

        <div class="item_notification <?= (!$notification['viewed'])?'new':''; ?> ">
           <p>
           <?php if(!$notification['viewed']){ ?>
             <i class="pull-right">New</i>
           <?php } ?>
             <?php echo $status; ?> <b><?=$notification['order']?></b> <?php echo $vid; ?> <b><?=$notification['date_added']?></b><br>
        <?php echo $zmin; ?> : &nbsp<b>
        <?php if($notification['status'] == 'В обробці'){ ?>
                <?= $decoration; ?>
        <?php }elseif($notification['status'] == 'Відправлено'){ ?>
          <?= $posted; ?>
          <?php }elseif($notification['status'] == 'Доставлено'){ ?>
          <?= $delivered; ?>
          <?php }elseif($notification['status'] == 'Оформлено'){ ?>
          <?= $decorated; ?>
          <?php } ?>
        </b>
           </p>
           <p>
             <?php if(strlen($notification['comment'])>0){ ?>
             <?php echo $com; ?> :&nbsp <?=$notification['comment'] ?>
             <?php }else{ ?>
             <?php echo $comvid; ?>
             <?php } ?>
           </p>

           <i class="pull-right"><?=$notification['date']?></i>
           <div style="clear:both;"> </div>

        </div>
        <?php } ?>
      <?php echo $content_bottom; ?></div>
      <div class="clearfix"></div>
    <?php echo $column_right; ?>
  </div>
  </div>
</div>
</div>
<?php echo $footer; ?>
