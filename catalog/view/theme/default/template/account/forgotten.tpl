<?php echo $header; ?>
<div class="container">
  <div class="legend__inner hidden-xs">
                                <span class="legend__information about_villa-title">
                                  <ul class="breadcrumbmy breadcrumbmy_KP breadcrumbmy-margin-style">
                                    <?php
      $i = 1;
      $count = count($breadcrumbs);
    ?>
                                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                                    <?php if($count == $i){ ?>
                                    <li><a class="breadcrumbmy-last-elem"><?php echo $breadcrumb['text']; ?></a></li>
                                    <?php }else{ ?>
                                    <li><a class="breadcrumbmy-first-elem"
                                           href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'].' - '; ?></a>
                                    </li>
                                    <?php } ?>
                                    <?php $i+=1; ?>
                                    <?php } ?>
                                  </ul>
                                </span>
                                <span class="legend__right-information l-ab_villa" style="width: 100%"></span>
	</div>

  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class=""><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8 col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content-KP-forgotten" class="panel panel-default"><?php echo $content_top; ?>
      <div class="cart-part1-div-style_KP">
        <h4 class="cart-part1-h4-style"><?php echo $heading_title; ?></h4>
      </div>
      <p style="padding: 20px"><?php echo $text_email; ?></p>
      <form style="padding: 20px" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <legend><?php echo $text_your_email; ?></legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control input-email_KP" />
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <div class="pull-left">
            <a href="<?php echo $back; ?>" class="btn btn-back button_cart_KP"><?php echo $button_back; ?></a>
          </div>
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-continue button_cart_KP" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>