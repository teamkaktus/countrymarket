<?php if (count($currencies) > 1) { ?>
<div class="text_currency_style">
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="currency">
  <div class="btn-group">
      <?php foreach ($currencies as $currency) { ?>
      <span class="style_currency"><?php echo $currency['symbol_left'].$currency['value']; ?></span>
      <?php } ?>
  </div>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
</div>
<?php } ?>
