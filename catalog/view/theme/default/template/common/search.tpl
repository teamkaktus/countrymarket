<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 style_search no_padding" >
    <div id="search" class="input-group" style="float: right; z-index: 9">
      <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control_style input-lg" />
      <span class="input-group-btn">
        <button type="button" class="btn btn-default btn-lg header_button_search"><i class="fa fa-search"></i></button>
      </span>
    </div>
</div>
