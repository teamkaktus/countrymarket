<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumbmy breadcrumbmy_KP breadcrumbmy-margin-style">
    <?php
      $i = 1;
      $count = count($breadcrumbs);
    ?>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php if($count == $i){ ?>
    <li><a class="breadcrumbmy-last-elem"><?php echo $breadcrumb['text']; ?></a></li>
    <?php }else{ ?>
    <li><a class="breadcrumbmy-first-elem" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'].' - '; ?></a></li>
    <?php } ?>
    <?php $i+=1; ?>
    <?php } ?>
  </ul>
  <div class=""><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content-success-KP" class="panel panel-default"><?php echo $content_top; ?>
      <div class="cart-part1-div-style_KP">
        <h4 class="cart-part1-h4-style"><?php echo $heading_title; ?></h4>
      </div>
      <div class="col-md-12" style="margin-top: 25px; padding: 0 20px">
        <?php echo $text_message; ?>
      </div>
      <div class="buttons float_right" style="margin-bottom: 25px !important; margin-right: 25px;" >
        <div class="container_button_continue_KP">
          <a href="<?php echo $continue; ?>" class="btn button_cart button_cart_KP "><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>