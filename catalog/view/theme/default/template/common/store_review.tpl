<div class="reviewpadding">
	<span class="legend__inner2">
                                <span class="legend__left2 l-ab_villa hidden-xs"></span>
                                <span class="legend__content2 about_villa-title mob_style"><?php echo $reviews_about_us; ?></span>
                                <span class="legend__right2 l-ab_villa hidden-xs"></span>
	</span>
	<div id="myCarousel" class="carousel slide container_review" data-ride="carousel" style="display: block; margin-top: 23px;    margin-bottom: 45px;" >
		<div class="carousel-inner" role="listbox">
			<?php $i = 0;  foreach ($reviews as $review) { ?>

			<div class="item <?php if ($i == 0) { ?>active<?php } ?>">
				<!-- <div class="col-sm-12 text-center"> -->
					<!-- <figure> -->
						<!-- <img src="<?php //echo $review['image']; ?>" alt="" class="kolo"> -->
					<!-- </figure> -->
				<!-- </div> -->
				<?php if(strlen($review['text']) < 160){ ?>
				<div class="text_text_container">
					<span class="text_text mob_text_20"><?php echo $review['text']; ?></span>
				</div>
				<?php }else{ ?>
				<div class="text_text_container">
					<span class="text_text mob_text_20"><?php echo $review['text']; ?> ...</span>
					<a class=" text text_2 mob_text_20"  href="index.php?route=review/store_review#review<?=$review['review_id']; ?>">Читати більше</a>
				</div>
				<?php } ?>
				<span class="text_author mob_style"><?php echo $review['author']; ?></span>


			</div>
			<?php $i++; } ?>

			<a class="left carousel-control container_carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="strilcka_vlivo" aria-hidden="true"></span>
				<span class="sr-only">Previous</span> </a>
			<a class="right carousel-control container_carousel-control" href="#myCarousel" role="button" data-slide="next">
				<span class="strilcka_vpravo" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>

</div>