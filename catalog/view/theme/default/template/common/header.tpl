<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=400, initial-scale=0.6">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $title; ?></title>
	<base href="<?php echo $base; ?>"/>
	<?php if ($description) { ?>
	<meta name="description" content="<?php echo $description; ?>"/>
	<?php } ?>
	<?php if ($keywords) { ?>
	<meta name="keywords" content="<?php echo $keywords; ?>"/>
	<?php } ?>
	<script src="../catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
	<link href="../catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
	<script src="../catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<link href="../catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css"/>
	<link href="../catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
	<link href="../catalog/view/theme/default/stylesheet/mystylesheet.css" rel="stylesheet">
	<link href="../catalog/view/theme/default/stylesheet/stylesCSS.css" rel="stylesheet">

	<script src="../catalog/view/javascript/auto-fix/js/jquery.autofix_anything.js" type="text/javascript"></script>
	<link href="../catalog/view/javascript/auto-fix/css/autofix_anything.css" rel="stylesheet">

	<script src="../catalog/view/javascript/header.js" type="text/javascript"></script>
	<script src="../catalog/view/javascript/bootbox.min.js" type="text/javascript"></script>


	<?php foreach ($styles as $style) { ?>
	<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
		  media="<?php echo $style['media']; ?>"/>
	<?php } ?>
	<script src="../../catalog/view/javascript/jQueryRotate.2.2.js" type="text/javascript"></script>
	<script src="../../catalog/view/javascript/common.js" type="text/javascript"></script>
	<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
	<?php } ?>
	<?php foreach ($scripts as $script) { ?>
	<script src="<?php echo $script; ?>" type="text/javascript"></script>
	<?php } ?>
	<?php foreach ($analytics as $analytic) { ?>
	<?php echo $analytic; ?>
	<?php } ?>
	<style>
		.rotatingHands {
			position: absolute;
			width: 100%;
			height: 100%;
			top: 0px;
			left: 0px;
		}

		#hours, #minutes, #seconds {
				position: absolute;
				display: block;
		}
		#clock{
			width: 100%;
			height: 100%;
			position: relative;
		}


		.rotatingHands2 {
			position: absolute;
			width: 100%;
			height: 100%;
			top: 0px;
			left: 0px;
		}

		#hours2, #minutes2, #seconds2 {
				position: absolute;
				display: block;
		}
		#clock2{
			width: 100%;
			height: 100%;
			position: relative;
		}



		.rotatingHands3 {
			position: absolute;
			width: 100%;
			height: 100%;
			top: 0px;
			left: 0px;
		}

		#hours3, #minutes3, #seconds3 {
				position: absolute;
				display: block;
		}
		#clock3{
			width: 100%;
			height: 100%;
			position: relative;
		}


		.rotatingHands4 {
			position: absolute;
			width: 100%;
			height: 100%;
			top: 0px;
			left: 0px;
		}

		#hours4, #minutes4, #seconds4 {
				position: absolute;
				display: block;
		}
		#clock4{
			width: 100%;
			height: 100%;
			position: relative;
		}

		.clock_container{
			width:80px;
			height:80px;
			margin:0 auto;
		}
	</style>
</head>
<body class="<?php echo $class;?> sviat_mob_text">
<nav class="top_style" id="top" style="margin: 0 0 22px 0;">
	<div class="container">
		<div class="col-xs-12 col-sm-4 col-md-4  col-lg-4 container_logo" style="padding-left: 0; margin-top: 25px;">
			<div id="logo">
				<?php if ($logo) { ?>
				<a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>"
													alt="<?php echo $name; ?>" class="img-responsive"/></a>
				<?php } else { ?>
				<h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
				<?php } ?>
			</div>
		</div>
		<div class="col-sm-2 col-md-2 col-lg-2 col-xs-3">
			<div class="clock_container">
				<div style="position:relative;">
					<div id="clock">
						<div class="rotatingHands"><img id="hours" src="../image/hours.png"  style="width:100%;" /></div>
						<div class="rotatingHands"><img id="minutes" src="../image/minutes.png"  style="width:100%;" /></div>
						<div class="rotatingHands"><img id="seconds" src="../image/seconds.png"  style="width:100%;" /></div>
						<img src="../image/clock.png"  style="width:100%;" />
					</div>
				</div>
			</div>
			<div class="country">London</div>
		</div>

		<div class="col-sm-2 col-md-2 col-lg-2 col-xs-3">
			<div class="clock_container">
				<div style="position:relative; ">
					<div id="clock2">
						<div class="rotatingHands2"><img id="hours2" src="../image/hours.png"  style="width:100%;" /></div>
						<div class="rotatingHands2"><img id="minutes2" src="../image/minutes.png"  style="width:100%;" /></div>
						<div class="rotatingHands2"><img id="seconds2" src="../image/seconds.png"  style="width:100%;" /></div>
						<img src="../image/clock.png"  style="width:100%;" />
					</div>
				</div>
			</div>
			<div class="country">Kyiv</div>
		</div>
		<div class="col-sm-2 col-md-2 col-lg-2 col-xs-3">
			<div class="clock_container">
				<div id="clock3">
					<div class="rotatingHands3"><img id="hours3" src="../image/hours.png"  style="width:100%;" /></div>
					<div class="rotatingHands3"><img id="minutes3" src="../image/minutes.png"  style="width:100%;" /></div>
					<div class="rotatingHands3"><img id="seconds3" src="../image/seconds.png"  style="width:100%;" /></div>
					<img src="../image/clock.png"  style="width:100%;" />
				</div>
			</div>
			<div class="country">New York</div>
		</div>

		<div class="col-sm-2 col-md-2 col-lg-2 col-xs-3">
			<div class="clock_container">
				<div id="clock4">
					<div class="rotatingHands4"><img id="hours4" src="../image/hours.png"  style="width:100%;" /></div>
					<div class="rotatingHands4"><img id="minutes4" src="../image/minutes.png"  style="width:100%;" /></div>
					<div class="rotatingHands4"><img id="seconds4" src="../image/seconds.png"  style="width:100%;" /></div>
					<img src="../image/clock.png"  style="width:100%;" />
				</div>
			</div>
			<div class="country">Berlin</div>
		</div>
	</div>
</nav>
<nav class="top_style" id="top">
	<div class="container">
		<div class="currency col-xs-12 col-sm-12 hidden-md hidden-lg">
			<span class="text_currency" ><?php echo $currency; ?></span>
		</div>
		<div class="language col-xs-5 col-sm-4 col-md-3 col-lg-2">
			<span class="text_language_2 header-ln-style"><?php echo $language_selection?><?php echo $language ?></span>
		</div>
		<div class="col-xs-7 col-sm-4 col-md-4 col-lg-3 container_select_country">
			<div class="text_select_country"><?php echo $select_country ; ?></div>
			<div class="btn-group ">
				<button  class="header-ln-style_1 btn btn-link dropdown-toggle button_sviat_mob" data-toggle="dropdown">
					<?php foreach ($stores as $store) { ?>
					<?php if ($current_store == $store['store_id']) { ?>
					<span class="text_language"><?php echo $store['name']; ?></span>
					<?php } ?>
					<?php } ?>
					</button>
				<ul class="dropdown-menu">
					<?php foreach ($stores as $store) { ?>
					<li><a href="<?php echo $store['href']; ?>"><?php echo $store['name']; ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="currency currency_KP hidden-xs hidden-sm col-md-5 col-lg-4 no_padding">
			<span class="text_currency"><?php echo $currency; ?></span>
		</div>
		<div class="login_and_register login_and_register_KP col-xs-12 col-sm-4 col-md-12 col-lg-3">
				<a style="padding-right: 10px" href="index.php?route=checkout/checkout"><img src="/image/catalog/cart_news.png" alt=""></a>
			<img src="/image/iconka_1.png">
			<?php if ($logged) { ?>
			<span class="text_login"><a href="../../index.php?route=account/edit"><?php echo $header_profile_text;
			echo ','.' '.$firstname; ?></a></span>
			<span class="text_register"><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></span>
			<?php }else{ ?>
			<span class="text_login"><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></span>
			<span class="text_register"><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></span>
			<?php } ?>
		</div>
	</div>
</nav>
<header class="header" style="margin-bottom: 25px;">
	<div class="container fix_style">
		<div class="telephone col-xs-6 col-sm-6 col-md-2 col-lg-2 no_padding">
			<div class="phone-icon-padding">
				<span class="icon_telephone_style"><a href="<?php echo $contact; ?>"><img
								src="/image/Telephone_icon.png"></a></span>
				<span class="text_telephone_style"><?php echo $telephone; ?></span>
			</div>
		</div>
		<div class=" div-menu-style hidden-xs hidden-sm col-md-7 col-lg-7 text-center no_padding div-menu-style_KP">
			<ul id="nav" class="no-padding">
				<li><a href="index.php?route=information/information&information_id=4"><span class="header-menu-text-style"><?php echo $about_us ; ?></span></a></li>
				<li><a href="index.php?route=information/information&information_id=6"><span class="header-menu-text-style_1"><?php echo $delivery ; ?></span></a></li>
				<li><a href="index.php?route=information/information&information_id=6"><span class="header-menu-text-style_1"><?php echo $offers ; ?></span></a>
					<ul style="z-index: 99999999999999999; background: #fff; padding: 0px; margin-left: 25px; opacity: 0.9;">
						<li ><a href="index.php?route=information/information&information_id=6"><span class="header-menu-text-style_4"><?php echo $payment ; ?></span></a></li>
						<li ><a href="index.php?route=information/information&information_id=6"><span class="header-menu-text-style_4"><?php echo $quality_control ; ?></span></a></li>
					</ul>
				</li>
				<li><a href="index.php?route=review/store_review"><span class="header-menu-text-style"><?php echo $reviews ; ?></span></a></li>
				<li><a href="/index.php?route=information/contact"><span class="header-menu-text-style_4"><?php echo $contacts ; ?></span></a></li>
			</ul>
		</div>
		<?php echo $search ; ?>

		<div class=" div-menu-style hidden-xs col-sm-12 hidden-md hidden-lg text-center no_padding div-menu-style_KP">
			<ul id="nav" class="no-padding">
				<li><a href="index.php?route=information/information&information_id=4"><span class="header-menu-text-style"><?php echo $about_us ; ?></span></a></li>
				<li><a href="index.php?route=information/information&information_id=6"><span class="header-menu-text-style_1"><?php echo $delivery ; ?></span></a></li>
				<li><a href="index.php?route=information/information&information_id=6"><span class="header-menu-text-style_1"><?php echo $offers ; ?></span></a>
					<ul style="z-index: 99999999999999999; background: #fff; padding: 0px; margin-left: 25px; opacity: 0.9;">
						<li ><a href="index.php?route=information/information&information_id=6"><span class="header-menu-text-style_4"><?php echo $payment ; ?></span></a></li>
						<li ><a href="index.php?route=information/information&information_id=6"><span class="header-menu-text-style_4"><?php echo $quality_control ; ?></span></a></li>
					</ul>
				</li>
				<li><a href="index.php?route=review/store_review"><span class="header-menu-text-style"><?php echo $reviews ; ?></span></a></li>
				<li><a href="/index.php?route=information/contact"><span class="header-menu-text-style_4"><?php echo $contacts ; ?></span></a></li>
			</ul>
		</div>
		<nav class="navbar navbar-default hidden-sm hidden-md hidden-lg"  role="navigation" style="background-color: transparent; border: none; position: relative; z-index: 1">
				<button style="position: absolute; z-index: 99; right: 50%; margin-top: -7px;" type="button" class="navbar-toggle collapsed mini_menu_KP" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

			<div class="collapse navbar-collapse mini_menu_KP" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav" style="width: 100%; z-index: 1">
					<li><a style="border-bottom: 1px green dashed; " class="text_storinok text-center" href="index.php?route=information/information&information_id=4"><span><?php echo $about_us ; ?></span></a></li>
					<li><a style="border-bottom: 1px green dashed; padding-left: 20px" class="text_storinok text-center" href="index.php?route=information/information&information_id=6"><span class=""><?php echo $delivery ; ?></span></a></li>
					<li><a style="border-bottom: 1px green dashed;" class="text_storinok hidden-xs hidden-sm hidden-md text-center" href="index.php?route=information/information&information_id=7"><span ><?php echo $payment ; ?></span></a></li>
					<li><a style="border-bottom: 1px green dashed;" class="text_storinok text-center" href="/index.php?route=information/contact"><span ><?php echo $contacts ; ?></span></a></li>
					<li><a style="border-bottom: 1px green dashed;" class="text_storinok text-center" href="index.php?route=review/store_review"><span ><?php echo $reviews ; ?></span></a></li>
					<li><a style="border-bottom: 1px green dashed; " class="text_storinok hidden-lg text-center" href="index.php?route=information/information&information_id=7"><span><?php echo $quality_control ; ?></span></a></li>
					<li><a class="text_storinok text-center" href="index.php?route=information/information&information_id=6"><span><?php echo $offers ; ?></span></a>
				</ul>
			</div><!-- /.navbar-collapse -->
		</nav>
	</div>
<div style="height: 102px">
    <nav class="top_style fix_top" id="top_slyder" style="height: 102px">

        <div class="container ">
            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-10 masterBlock no-padding">
                <div id="carousel_header" class="owl-carousel owl-theme menuContainer">
                    <?php foreach ($categories as $category_1) { ?>
                    <div class="item menu" id="<?php echo $category_1['category_id']?>" data-cont="<?php echo $category_1['category_id']?>" data-hash="<?php echo $category_1['category_id']?>" style="text-align: center; border: 1px solid transparent;">
                        <div class="">
                            <a href="<?php echo $category_1['href'] ; ?>">
                                <img class="header_image" src="../../image/<?php echo $category_1['image'] ; ?>">
                            </a>
                        </div>
                        <a href="<?php echo $category_1['href'] ; ?>"><span class="text_style_owl"><?php echo $category_1['name'] ; ?></span></a>
                    </div>
                    <?php } ?>
                </div>
                <?php foreach ($categories as $category_1) { ?>
                <div class="categories_child content" data-conttab="<?php echo $category_1['category_id']?>">
                    <div class="leftTopLine" data-linel="<?php echo $category_1['category_id']?>"></div>
                    <div class="rightTopLine" data-liner="<?php echo $category_1['category_id']?>"></div>

                    <?php foreach ($category_1['children'] as $category_2) { ?>
                    <div class="subMenu">
                        <a href="<?= $category_2['href']; ?>" class="category_link">
                            <span class="text_style_owl_KP"><?= $category_2['name']; ?></span>
                        </a>
                        <?php foreach ($category_2['children'] as $category_3) { ?>
                        <div class="">
                            <a href="<?= $category_3['href']; ?>">
                                <span class="text_style-KP"><?= $category_3['name']; ?></span>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>

                </div>
                <?php } ?>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2 no-padding" style="background-color: #fff; height: 102px; border-radius: 0 3px  3px 0;    opacity: 0.8;"></div>
        </div>
    </nav>
</div>
</header>