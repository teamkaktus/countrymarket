<div class="">
	<div style="display: none">
		<audio id="beep-one" controls="controls" preload="auto" style="    position: fixed; visibility: hidden;">
			<source src="/image/yvd.mp3"></source>
		</audio>
	</div>
	<div id="cart" class="cartview cart toggle_menu_wrap cart_KP" style="position: relative">
		<div class="cart_container_KP">

		</div>
		<ul class="pull-right toggle_menu_cont toggle_menu_cont_22 no_padding" style="box-shadow: 0 0 5px 1px rgba(0, 0, 0, 0.3);">

			<?php if ($products || $vouchers) { ?>
			<li><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cls_border_2 no-padding no-margin">
					<div class="row no-margin">
						<div class="col-lg-5 col-xs-5 col-md-5 col-sm-5 text-left" style="margin-top: 20px; margin-bottom: 20px;">
							<i class="icon_cart_1"></i><span class="text_shopping_cart mob_style"><?php echo $text_shopping_cart; ?></span>
						</div>
						<div class="col-lg-7 col-xs-7 col-md-7 col-sm-7 text-right" style="margin-top: 21px; margin-bottom: 20px; padding-right: 22px">
							<span class="text_shopping_cart2 mob_style">
								<a href="#" onclick="$('#cart > ul').load('index.php?route=checkout/cart/remove&remove_all' + ' #cart > *'); location.reload(true);">Очистити корзину</a>
								</span>
						</div>
					</div>
				</div>
				<table class="table carttable cart-tbody-display">
					<?php foreach ($products as $product) { ?>
					<tr style="display: block">
						<td style="display: block; border-top: none !important; padding-bottom: 0;" class="block_image_cart"><?php if ($product['thumb']) { ?>
							<div style="padding-bottom: 10px; border-bottom: 1px dashed #c8c8c8;">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" >
										<a href="<?php echo $product['href']; ?>">
											<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="image_cart-style" />
										</a>
									</div>
									<?php } ?>
									<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
										<div class="row">
											<div class="title-cart-style col-lg-10 col-md-9 col-sm-9 col-xs-9 " style="margin-bottom: 7px; padding-right: 0">
												<a class="cart_product_name mob_style" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
												<input type="hidden" class="added_product-id" value="<?=$product['product_id'];?>">
											</div>
											<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no_padding" style="float: right;">
												<button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>', <?=$product['product_id']?>);" title="<?php echo $button_remove; ?>" class="cart-icon_X-style" style="background-size: 80% 100%;"></button>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="fygura-47" style="height: 30px; width: 54px; float: left; background-image: url('../image/fygura-47.png'); background-size: 100% 100%;">
													<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no_padding cart-icon-div-style">
														<i class="charges col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding mob_style" style="bottom: 3px; cursor: pointer" onclick="quantity_dec(<?=$product['key'];?>, <?=$product['product_id'];?>)">-</i>
													</div>
													<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 no_padding cart-style_quantity_1">
														<input style="border: 0; background-color: rgba(0,0,0,0)" type="text" name="quantity[<?php echo $product['key']; ?>]"
															   value="<?php echo $product['quantity']; ?>" size="1" class="text-center input-quantity mob_style" oninput="quantity_onchange(<?=$product['key'];?>, <?=$product['product_id'];?>)"/>
													</div>
													<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no_padding cart-icon-div-style_1">
														<i class="charges col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding mob_style" style="cursor: pointer"  onclick="quantity_inc(<?=$product['key'];?>, <?=$product['product_id'];?>)">+</i>
													</div>
													<div style="clear: both;"></div>
												</div>
											</div>
											<div class="col-lg-8">
												<div class="no_padding text-right" style="top: 5px;;">
													<span class="price-cart cart-price-product-total mob_style"><?php echo $product['total']; ?></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div style="clear: both;"></div>
							</div>
						</td>
					</tr>
					<?php } ?>
					<?php foreach ($vouchers as $voucher) { ?>
					<tr>
						<td class="text-center"></td>
						<td class="text-left"><?php echo $voucher['description']; ?></td>
						<td class="">x&nbsp;1</td>
						<td class=""><?php echo $voucher['amount']; ?></td>
						<td class="text-center text-danger">
							<button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs">
								<i class="fa fa-times"></i>
							</button>
						</td>
					</tr>
					<?php } ?>
				</table>
			</li>
			<li style="padding: 0 18px">
				<table class="table total" style="margin-bottom: 0px !important;">
					<tr>
                    <td class="text-left description-cart-text-style mob_text_20" style="border: none">
							<?php echo $text_number; ?>
						</td>
						<td class="text-right description-cart-text-style mob_text_20" style="border: none">
							<?php echo $quantitytotal; ?> шт.
						</td>
					</tr>
					<tr >
                    <td class="text-left description-cart-text-style mob_text_20" style="border: none">
						<?php echo $text_weight; ?>
						</td>
						<td class="text-right description-cart-text-style mob_text_20" style="border: none">
							<?php echo $weighttotal; ?>кг.
						</td>
					</tr>
                   <tr class="showhim">
                    <td class="text-left description-cart-text-style mob_text_20" style="border: none; ">
						<?php echo $text_delivery; ?>
						</td>
						<td class="text-right description-cart-text-style blockdostf" style="border: none; width: 20%;">
							<div class="ok"> <?php echo $parcel + $parcelpos; ?>$</div>
							<div class="showme">
								<?php if($parcelpos==0){ ?>
								<?php echo $parcel; ?>$
								<?php }else{ ?>
								<?php echo $parcel.'$ + '.$parcelpos; ?>$
								<?php } ?>
							</div>

						</td>


					</tr>
					<tr class="showhimq">
                    <td class="text-left description-cart-text-style mob_text_20" style="border: none; ">
						<?php echo $text_commission; ?>
						</td>
						<td class="text-right description-cart-text-style mob_text_20" style="border: none; width: 20%;">
							<div class="okq mob_text_20">
								<?php echo $commission + $commissionpos; ?>$
							</div>
							<div class="showmeq mob_text_20">
								<?php if($commissionpos==0){ ?>
								<?php echo $commission; ?>$
								<?php }else{ ?>
								<?php echo $commission.'$ + '.$commissionpos; ?>$
								<?php } ?>

							</div>
						</td>
					</tr>
                   <tr >
                    <td class="text-left description-cart-text-style mob_text_20" style="border: none">
						<?php echo $text_amount; ?>
						</td>
						<td class="text-right description-cart-text-style mob_text_20" style="border: none">
							<?php echo $totals; ?>
						</td>
					</tr>
					<tr>
						<td class="cls_border_3"></td>
					</tr>
                   <tr style="height: 50px;" class="cart-border-none">
                    <td class="text-left total-cart-title-style mob_text_20" style=" padding-top: 10px !important; width: 43%; padding-left: 8px !important;">
							<?php echo $cart_total_price; ?>
						</td>
						<td class="text-right total-cart-title-style mob_text_20" style=" padding-top: 10px !important;">
							<?php echo $totalsUsd; ?>
						</td>
					</tr>
					<tr class="cart-border-none">
						<td class="text-left total-cart-title-style mob_text_20" style=" padding-top: 10px; width: 43%; padding-left: 8px !important;">
							Total price:
						</td>
						<td class="text-right total-cart-title-style mob_text_20" style=" padding-top: 10px">
							<?php echo $totalUsd; ?>$
						</td>
					</tr>
				</table>
				<div class="row" style="margin-right: -17px; margin-left: -17px; margin-bottom: -4px; padding-bottom: 10px; background-image: url('../catalog/view/theme/default/image/CartBottom.png'); background-size: 100% 100%;">
					<div class="" style="margin-bottom: 10px; text-align: center;">
						<a style="border-radius: 0;" class=" btn  button-cart_style" href="<?php echo $checkout; ?>">
							<span class="mob_style" style="">
								<?php echo $text_checkout; ?>
							</span>
						</a>
					</div>
				</div>
			</li>
			<?php } else { ?>
			<li>
				<div class="col-sm-12 col-xs-12 cls_border_2_KP no-padding cart_KP_2" style="box-shadow: none !important; position: relative">
                    <div class="cls_border_2" style="position: relative; border: none;     box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.3);">
                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5  text-center"
                             style="margin-top: 20px; margin-bottom: 20px;">
                            <i class="icon_cart_1"></i><span
                                    class="text_shopping_cart mob_style"><?php echo $text_shopping_cart; ?></span>
                        </div>
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 text-center"
                             style="margin-top: 21px; margin-bottom: 20px;">
                            <span class="text_shopping_cart2 mob_style"><?=$text_empty; ?></span>
                        </div>
                    </div>
                    <div class="text-center container_cart_2">
						<i class="icon_cart_2"></i>
					</div>
					<div class="text-center container_cart_1" >
						<span class="text_shopping_cart3"><?=$text_empty; ?></span>
					</div>
					<div class="style_cart_bottom"></div>
				</div>
			</li>
			<?php } ?>
		</ul>
	</div>
	<input type="hidden" id="text_btn-added" value="<?=$button_cart_added;?>">
	<input type="hidden" id="text_cart" value="<?=$button_cart;?>">
</div>
