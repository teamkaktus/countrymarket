

function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	} else { 			// Изменения для seo_url от Русской сборки OpenCart 2x
		var query = String(document.location.pathname).split('/');
		if (query[query.length - 1] == 'cart') value['route'] = 'checkout/cart';
		if (query[query.length - 1] == 'checkout') value['route'] = 'checkout/checkout';
		
		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}

$(document).ready(function() {
    
         ////clock 1             
                var angleSec = 0;
                var angleMin = 0;
                var angleHour = 0;

                $(document).ready(function () {
                        $("#seconds").rotate(angleSec);
                        $("#minutes").rotate(angleMin);
                        $("#hours").rotate(angleHour);
                });

                setInterval(function () {
                        $.ajax({
                            url: 'index.php?route=common/header/getdatatimezone',
                            type: 'post',
                            data: {timezone:'0'},
                            dataType: 'json',
                            beforeSend: function() {
                            }
                        }).done(function(result){
                                var d = new Date(result.date);
                                angleSec = (d.getSeconds() * 6);
                                $("#seconds").rotate(angleSec);

                                angleMin = (d.getMinutes() * 6);
                                $("#minutes").rotate(angleMin);

                                angleHour = ((d.getHours() * 5 + d.getMinutes() / 12) * 6);
                                $("#hours").rotate(angleHour);
                            
                        });

                }, 1000);
         /////
         ////clock 2             
                var angleSec = 0;
                var angleMin = 0;
                var angleHour = 0;

                $(document).ready(function () {
                        $("#seconds2").rotate(angleSec);
                        $("#minutes2").rotate(angleMin);
                        $("#hours2").rotate(angleHour);
                });

                setInterval(function () {
                        $.ajax({
                            url: 'index.php?route=common/header/getdatatimezone',
                            type: 'post',
                            data: {timezone:'+2'},
                            dataType: 'json',
                            beforeSend: function() {
                            }
                        }).done(function(result){
                                var d = new Date(result.date);
                                angleSec = (d.getSeconds() * 6);
                                $("#seconds2").rotate(angleSec);

                                angleMin = (d.getMinutes() * 6);
                                $("#minutes2").rotate(angleMin);

                                angleHour = ((d.getHours() * 5 + d.getMinutes() / 12) * 6);
                                $("#hours2").rotate(angleHour);
                            
                        });

                }, 1000);
         /////
         ////clock 2             
                var angleSec = 0;
                var angleMin = 0;
                var angleHour = 0;

                $(document).ready(function () {
                        $("#seconds3").rotate(angleSec);
                        $("#minutes3").rotate(angleMin);
                        $("#hours3").rotate(angleHour);
                });

                setInterval(function () {

                        $.ajax({
                            url: 'index.php?route=common/header/getdatatimezone',
                            type: 'post',
                            data: {timezone:'-5'},
                            dataType: 'json',
                            beforeSend: function() {
                            }
                        }).done(function(result){
                                var d = new Date(result.date);
                                angleSec = (d.getSeconds() * 6);
                                $("#seconds3").rotate(angleSec);

                                angleMin = (d.getMinutes() * 6);
                                $("#minutes3").rotate(angleMin);

                                angleHour = ((d.getHours() * 5 + d.getMinutes() / 12) * 6);
                                $("#hours3").rotate(angleHour);
                            
                        });
                }, 1000);
         /////
         ////clock 4             
                var angleSec = 0;
                var angleMin = 0;
                var angleHour = 0;

                $(document).ready(function () {
                        $("#seconds4").rotate(angleSec);
                        $("#minutes4").rotate(angleMin);
                        $("#hours4").rotate(angleHour);
                });

                setInterval(function () {
                        $.ajax({
                            url: 'index.php?route=common/header/getdatatimezone',
                            type: 'post',
                            data: {timezone:'+1'},
                            dataType: 'json',
                            beforeSend: function() {
                            }
                        }).done(function(result){
                                var d = new Date(result.date);
                                angleSec = (d.getSeconds() * 6);
                                $("#seconds4").rotate(angleSec);

                                angleMin = (d.getMinutes() * 6);
                                $("#minutes4").rotate(angleMin);

                                angleHour = ((d.getHours() * 5 + d.getMinutes() / 12) * 6);
                                $("#hours4").rotate(angleHour);
                            
                        });

                }, 1000);
         /////
         
         
         

    
    
	// Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();

		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});

	// Currency
	$('#currency .currency-select').on('click', function(e) {
		e.preventDefault();

		$('#currency input[name=\'code\']').attr('value', $(this).attr('name'));

		$('#currency').submit();
	});
//Store
	$('#store .currency-select').on('click', function(e) {
		e.preventDefault();

		$('#store input[name=\'code\']').attr('value', $(this).attr('name'));

		$('#store').submit();
	});
	// Language
	$('#language a').on('click', function(e) {
		e.preventDefault();

		$('#language input[name=\'code\']').attr('value', $(this).attr('href'));

		$('#language').submit();
	});

	/* Search */
	$('#search input[name=\'search\']').parent().find('button').on('click', function() {
            
		url = $('base').attr('href') + 'index.php?route=product/search';

		var value = $('input[name=\'search\']').val();
		if (value) {
                    url += '&search=' + encodeURIComponent(value);
		}

		location = url;
	});
        
        
	$('#search input[name=\'search\']').on('keydown', function(e) {
		if (e.keyCode == 13) {
			$('header input[name=\'search\']').parent().find('button').trigger('click');
		}
	});
        
        
        $('#search_m input[name=\'search_m\']').parent().find('button').on('click', function() {

                url = $('base').attr('href') + 'index.php?route=product/search';

                var value = $('input[name=\'search_m\']').val();
                if (value) {
                    url += '&search=' + encodeURIComponent(value);
                }

                location = url;
        });
                
	// Menu
	$('#menu .dropdown-menu').each(function() {
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();

		var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 5) + 'px');
		}
	});

	// Product List
	$('#list-view').click(function() {
		$('#content .product-grid > .clearfix').remove();

		//$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');
		$('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');

		localStorage.setItem('display', 'list');
	});

	// Product Grid
	$('#grid-view').click(function() {
		// What a shame bootstrap does not take into account dynamically loaded columns
		cols = $('#column-right, #column-left').length;

		if (cols == 2) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols == 1) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
		}

		 localStorage.setItem('display', 'grid');
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
	} else {
		$('#grid-view').trigger('click');
	}

	// Checkout
	$(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function(e) {
		if (e.keyCode == 13) {
			$('#collapse-checkout-option #button-login').trigger('click');
		}
	});

	// tooltips on hover
	$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

	// Makes tooltips work on ajax generated content
	$(document).ajaxStop(function() {
		$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
	});

    if($(".added_product-id")) {
    	var added_text = $("#text_btn-added").val();
        $(".added_product-id").each(function (index, elem) {
        	$(".product_" + $(elem).val() + " .button_add_cart").text(added_text);
        })
    }

});

// Cart add remove functions
var cart = {
	'add': function(product_id, quantity) {

		$.ajax({
			url: 'index.php?route=checkout/cart/ajaxWeight',
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + (typeof( $('#box_quantity_'+ product_id +' .input-quantity').val()) != 'undefined' ? $('#box_quantity_'+ product_id +' .input-quantity').val() : 1),
			dataType: 'json',
			success: function(json) {
				
				if(json.status==1){
					$.ajax({
						url: 'index.php?route=checkout/cart/add',
						type: 'post',
						data: 'product_id=' + product_id + '&quantity=' + (typeof( $('#box_quantity_'+ product_id +' .input-quantity').val()) != 'undefined' ? $('#box_quantity_'+ product_id +' .input-quantity').val() : 1),
						dataType: 'json',
						beforeSend: function() {
							$('#cart > button').button('loading');
						},
						complete: function() {
							$('#cart > button').button('reset');
						},
						success: function(json) {
							var beepOne = $("#beep-one")[0];
							beepOne.play();
							$('#cart > ul').load('index.php?route=common/cart/info ul li');
							$('.alert, .text-danger').remove();

							var added_text = $("#text_btn-added").val();
							$(".product_" + product_id + " .button_add_cart").text(added_text);

							if (json['redirect']) {
								location = json['redirect'];
							}

							var tp = json.total;
							$('#blockcart').html(tp);
							if (json['success']) {
								//	$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

								// Need to set timeout otherwise it wont update the total


								//	$('html, body').animate({ scrollTop: 0 }, 'slow');

								$('#cart > ul').load('index.php?route=common/cart/info ul li');


								//  console.log(json['total']);
							}
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}else{
					bootbox.dialog({
						message: json.text+'-----------------',
						buttons: {
							success: {
								label: 'продовжити',
								className: "cart_style2_style",
								callback: function() {
									$.ajax({
										url: 'index.php?route=checkout/cart/add',
										type: 'post',
										data: 'product_id=' + product_id + '&quantity=' + (typeof( $('#box_quantity_'+ product_id +' .input-quantity').val()) != 'undefined' ? $('#box_quantity_'+ product_id +' .input-quantity').val() : 1),
										dataType: 'json',
										beforeSend: function() {
											$('#cart > button').button('loading');
										},
										complete: function() {
											$('#cart > button').button('reset');
										},
										success: function(json) {
											var beepOne = $("#beep-one")[0];
											beepOne.play();
											$('#cart > ul').load('index.php?route=common/cart/info ul li');
											$('.alert, .text-danger').remove();

											var added_text = $("#text_btn-added").val();
											$(".product_" + product_id + " .button_add_cart").text(added_text);

											if (json['redirect']) {
												location = json['redirect'];
											}

											var tp = json.total;
											$('#blockcart').html(tp);
											if (json['success']) {
												//	$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

												// Need to set timeout otherwise it wont update the total


												//	$('html, body').animate({ scrollTop: 0 }, 'slow');

												$('#cart > ul').load('index.php?route=common/cart/info ul li');


												//  console.log(json['total']);
											}
										},
										error: function(xhr, ajaxOptions, thrownError) {
											alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
										}
									});
								}
							},
							main: {
								label: 'відмінити',
								className: "cart_style2_style",
							}
						}
					});

				}

			}

		});

	},
	'update': function(key, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/edit',
			type: 'post',
			data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				var tp = json.totals;
    $('#blockcart').html('<span id="cart-total">' + tp + ' </span>');
				
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
                  
                      var tp = json.totals;
            $('#blockcart').html('<span id="cart-total">' + tp + ' </span>');
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
	'remove': function(key, id) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
				success: function(json) {
				/////////< Не видаляти///////////
                    var text_cart = $("#text_cart").val();
                    $(".product_" + id + " .button_add_cart").text(text_cart);
                /////////Не видаляти >///////////
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
                       var tp = json.total;
               $('#blockcart').html('<span id="cart-total"> ' + tp + ' </span>');
                     $('#contentc').load('index.php?route=checkout/checkout #contentcart');
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	}
}

var voucher = {
	'add': function() {

	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total">' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	}
}

var wishlist = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

				$('#wishlist-total span').html(json['total']);
				$('#wishlist-total').attr('title', json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
	'remove': function() {

	}
}

var compare = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=product/compare/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					$('#compare-total').html(json['total']);

					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
	'remove': function() {

	}
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
	e.preventDefault();

	$('#modal-agree').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-agree').modal('show');
		}
	});
});

// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			this.timer = null;
			this.items = new Array();

			$.extend(this, option);

			$(this).attr('autocomplete', 'off');

			// Focus
			$(this).on('focus', function() {
				this.request();
			});

			// Blur
			$(this).on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);
			});

			// Keydown
			$(this).on('keydown', function(event) {
				switch(event.keyCode) {
					case 27: // escape
						this.hide();
						break;
					default:
						this.request();
						break;
				}
			});

			// Click
			this.click = function(event) {
				event.preventDefault();

				value = $(event.target).parent().attr('data-value');

				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			}

			// Show
			this.show = function() {
				var pos = $(this).position();

				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left
				});

				$(this).siblings('ul.dropdown-menu').show();
			}

			// Hide
			this.hide = function() {
				$(this).siblings('ul.dropdown-menu').hide();
			}

			// Request
			this.request = function() {
				clearTimeout(this.timer);

				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			}

			// Response
			this.response = function(json) {
				html = '';

				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i]['value']] = json[i];
					}

					for (i = 0; i < json.length; i++) {
						if (!json[i]['category']) {
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						}
					}

					// Get all the ones with a categories
					var category = new Array();

					for (i = 0; i < json.length; i++) {
						if (json[i]['category']) {
							if (!category[json[i]['category']]) {
								category[json[i]['category']] = new Array();
								category[json[i]['category']]['name'] = json[i]['category'];
								category[json[i]['category']]['item'] = new Array();
							}

							category[json[i]['category']]['item'].push(json[i]);
						}
					}

					for (i in category) {
						html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

						for (j = 0; j < category[i]['item'].length; j++) {
							html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
						}
					}
				}

				if (html) {
					this.show();
				} else {
					this.hide();
				}

				$(this).siblings('ul.dropdown-menu').html(html);
			}

			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

		});
	}
})(window.jQuery);

// update cart
var update_cart = function (key, quantity, id) {
	
	$.ajax({
		url: 'index.php?route=checkout/cart/ajaxWeight',
		type: 'post',
		data: 'key=' + key +'&product_id=' + id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
		dataType: 'json',
		success: function(json) {
			if(json.status==1){
				$.ajax({
					url: 'index.php?route=checkout/cart/ajaxEdit',
					type: 'post',
					data: {
						quantity: (typeof(quantity) != 'undefined' ? quantity : 1),
						key: key
					},

					//dataType: 'json',
					beforeSend: function () {
						console.log('beforeSend');

					},
					complete: function () {

						console.log('complete');

					},
					success: function (json) {


						var tp = json.totals;
						$('#blockcart').html('<span id="cart-total"> ' + tp + ' </span>');
						var beepOne = $("#beep-one")[0];
						beepOne.play();
						$('#cart > ul').load('index.php?route=common/cart/info ul li');
						$('#contentc').load('index.php?route=checkout/checkout #contentcart');



					}
				});
			}else{
				bootbox.dialog({
					message: json.text+'------',
					buttons: {
						success: {
							label: 'продовжити',
							className: "cart_style2_style",
							callback: function() {
								$.ajax({
									url: 'index.php?route=checkout/cart/ajaxEdit',
									type: 'post',
									data: {
										quantity: (typeof(quantity) != 'undefined' ? quantity : 1),
										key: key
									},

									//dataType: 'json',
									beforeSend: function () {
										console.log('beforeSend');

									},
									complete: function () {

										console.log('complete');

									},
									success: function (json) {


										var tp = json.totals;
										$('#blockcart').html('<span id="cart-total"> ' + tp + ' </span>');
										var beepOne = $("#beep-one")[0];
										beepOne.play();
										$('#cart > ul').load('index.php?route=common/cart/info ul li');
										$('#contentc').load('index.php?route=checkout/checkout #contentcart');



									}
								});
							}
						},
						main: {
							label: 'відмінити',
							className: "cart_style2_style",
						}
					}
				});

			}

		}

	});



};

function quantity_dec(key, id) {
    var q_obj = $("input[name='quantity[" + key + "]']");
    var q_count = q_obj.val();
    var q = 1;
    if(q_count > 1){
        q = parseInt(q_count) - 1;
        update_cart(key, q, id);
        q_obj.val(q)
    }




}
function quantity_inc(key, id) {
    var q_obj = $("input[name='quantity[" + key + "]']");
    var q_count = q_obj.val(),
        q = parseInt(q_count) + 1;

    update_cart(key, q, id);
    q_obj.val(q)
}

function quantity_onchange(key, id) {
    
    var q_obj = $("input[name='quantity[" + key + "]']");
    var q_count = q_obj.val();
        if(q_count==''){q_count=0;}
        q = parseInt(q_count);
	
    update_cart(key, q, id);
    q_obj.val(q)
}


