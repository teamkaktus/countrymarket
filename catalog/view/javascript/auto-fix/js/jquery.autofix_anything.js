$(document).ready(function() {
    var fix_top = $('.fix_top').offset().top;
    $(window).on('scroll , resize, load', function() {
        var columnLeft = $('.fix_top').innerWidth(),
            headerHeight = $('.header').innerHeight(),
            footerHeight = $('.footer').innerHeight(),
            topMenu = $('.fix_top').offset().top - $(window).scrollTop(),
            topMenuWrap = $('.fix_top').offset().top - $(window).scrollTop(),
            footerPosition = $('.footer').offset().top - $(window).scrollTop() - fix_top,
            wHeight = window.innerHeight;

        if(($(window).scrollTop() - fix_top) >= topMenuWrap && window.innerWidth >= 400) {
            $('.fix_top').addClass('autofix_sb');
            $('.fix_top').css({

            });

            if($('.fix_top').height() >= footerPosition ){
                $('.fix_top').css({
                    'max-height': $('.footer').offset().top - $(window).scrollTop()
                });
            }
        }
        else{
            $('.fix_top').removeClass('autofix_sb');
            $('.fix_top').css({

            });
        }
    });


var cart_home_fix = $('.fix_top').offset().top;
    $(window).on('scroll , resize, load', function() {
         var cart_block = $('#cart');
        if(cart_block.length){
              var columnLeft = $('.cart_home_fix').innerWidth(),
                  headerHeight = $('.header').innerHeight(),
                  footerHeight = $('.footer').innerHeight(),
                  topMenu = $('.cart_home_fix').offset().top - $(window).scrollTop(),
                  topMenuWrap = $('.fix_top').offset().top - $(window).scrollTop(),
                  wHeight = window.innerHeight;

                  if(($(window).scrollTop() - cart_home_fix) >= topMenuWrap && window.innerWidth < 992 ){
                      $('.cart_home_fix').addClass('autofix_cart_home_mob');
                  }else if(($(window).scrollTop() - cart_home_fix) >= topMenuWrap && window.innerWidth >= 795) {
                      $('.cart_home_fix').addClass('autofix_cart_home');
                  }else{
                      $('.cart_home_fix').removeClass('autofix_cart_home');
                      $('.cart_home_fix').css({});
                  }
          }
    });
})

$(window).resize(function() {
     var cart_block = $('#cart');
    if(cart_block.length){

        if($( window ).width() < '992'){
            $('.cart_home_fix').removeClass('autofix_cart_home');
            $('.cart_home_fix').addClass('autofix_cart_home_mob');
        }else{
            $('.cart_home_fix').removeClass('autofix_cart_home_mob');
            $('.cart_home_fix').addClass('autofix_cart_home');
        }

        var cart_home_fix = $('.fix_top').offset().top;
        var columnLeft = $('.cart_home_fix').innerWidth(),
            headerHeight = $('.header').innerHeight(),
            footerHeight = $('.footer').innerHeight(),
            topMenu = $('.cart_home_fix').offset().top - $(window).scrollTop(),
            topMenuWrap = $('.fix_top').offset().top - $(window).scrollTop(),
            wHeight = window.innerHeight;

            if(($(window).scrollTop() - cart_home_fix) >= topMenuWrap && window.innerWidth < 992 ){
                  console.log('1');
                $('.cart_home_fix').addClass('autofix_cart_home_mob');
            }else if(($(window).scrollTop() - cart_home_fix) >= topMenuWrap && window.innerWidth >= 795) {
                $('.cart_home_fix').addClass('autofix_cart_home');
                  console.log('2');
            }else{
                $('.cart_home_fix').removeClass('autofix_cart_home');
                $('.cart_home_fix').css({});
                  console.log('3');
            }

    }
});