$(document).ready(function() {
    $('#carousel_header').owlCarousel({
        nav:true,
        navText: ['', '<img src="../../../image/catalog/demo/Strilka_vpravo.png" />'],
        loop:true,
        // margin:5,
        URLhashListener:true,
        startPosition: 'URLHash',
        dots: false,
        responsive:{
            500:{
                items:4
            },
            600:{
                items:5
            },
            768:{
                items:6
            },
            992:{
                items:8
            },
            1200:{
                items:9
            }
        },
        onInitialized: callback
    })

    function callback(event) {
        var selItem;
        $(".menu").each(function () {
            $(this).hover(function () {
                    $(this).css('border', '2px solid transparent');
                    var thisM = $(this);
                    selItem = this;
                    var parrent = $(".menuContainer")[0];
                    var id = this.getAttribute("data-cont");
                    var block = $('[data-conttab="' + id + '"]')[0];
                    var blocks = $('[data-conttab="' + id + '"]');
                    if(blocks.find('.subMenu').length){
                        $(this).css('border', '2px solid #69b206');
                        thisM.addClass('activeG');
                        block.style.display = "block";
                        var this_coords = this.parentNode.getBoundingClientRect();
                        var parent_coords = parrent.parentNode.getBoundingClientRect();
                        var block_coords = block.getBoundingClientRect();
                        var contlineL = $('[data-linel="' + id + '"]')[0];
                        var contlineR = $('[data-liner="' + id + '"]')[0];

                        block.style.top = this.offsetHeight - 3;


                        if (block.offsetWidth - 16 > (this_coords.right - parent_coords.left) &&
                            block.offsetWidth - 16 < (parent_coords.right - this_coords.left)) {
                            //block.style.left = -1*(8)+15+(this_coords.left-parent_coords.left)+'px';
                            //block.style.left = -7 + 'px';
                            block.style.left = 0 + 'px';
                            block.style.right = '';
                            block.style.top = '102px';
                            block.style.border = '2px solid #69b206';
                            contlineL.style.width = this_coords.left - parent_coords.left + 7 + 'px';
                            contlineR.style.width = block_coords.right - this_coords.right + 'px';
                        } else if (block.offsetWidth - 16 < (this_coords.right - parent_coords.left) &&
                            block.offsetWidth - 16 < (parent_coords.right - this_coords.left)) {
                           // block.style.left = this_coords.left - parent_coords.left - 8 + 'px';
                            block.style.left = this_coords.left - parent_coords.left - 0 + 'px';
                            block.style.right = '';
                            block.style.top = '102px';
                            block.style.border = '2px solid #69b206';
                            contlineL.style.width = 8 + 'px';
                            contlineR.style.width = block_coords.right - this_coords.right + 'px';
                        } else if (block.offsetWidth - 16 < (this_coords.left - parent_coords.left) &&
                            block.offsetWidth - 16 > (parent_coords.right - this_coords.left)) {
                            //block.style.left = this_coords.left-parent_coords.left -8 + 'px';
                           // block.style.right = -15 + 'px';
                            block.style.right = 0 + 'px';
                            block.style.left = '';
                            block.style.top = '102px';
                            block.style.border = '2px solid #69b206';
                            contlineL.style.width = this_coords.left - block_coords.left + 'px';
                            contlineR.style.width = parent_coords.right - this_coords.right + 15 + 'px';
                        }
                    }

                }, function () {
                    var thisS = this;
                    var id = thisS.getAttribute("data-cont");
                    var block = $('[data-conttab="' + id + '"]')[0];
                    var blockM = $('[data-conttab="' + id + '"]');
                    var thisM = $(this);
                    
                    setTimeout(function () {
                        if (blockM.is(':hover')) {
                        } else {
                            thisM.removeClass('activeG');
                            thisM.css('border', '2px solid transparent');
                            block.style.display = "none";
                        }
                    }, 100);
                }
            );
        });
        
        
        $('.subMenu').parent().hover(function () {
            
        },function(){
                $('.menu').removeClass('activeG');
                $('.menu').css('border', '2px solid transparent');
        });
        
        $(".content").each(function () {
            $(this).hover(function () {
                this.style.display = "block";
                $(selItem).addClass("menuHovered");
            }, function () {
                this.style.display = "none";
                $(selItem).removeClass("menuHovered");
            });
        });

    }

});
