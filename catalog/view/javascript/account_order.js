$(document).ready(function(){
    
    $(document).on('click','.showDetailOrder',function(){
        $(this).removeClass('showDetailOrder');
        $(this).addClass('hideDetailOrder');
        $(this).parent().parent().parent().find('.blockMoreInfo').show();
    });
    $(document).on('click','.hideDetailOrder',function(){
        $(this).removeClass('hideDetailOrder');
        $(this).addClass('showDetailOrder');
        $(this).parent().parent().parent().find('.blockMoreInfo').hide();
    });
    
    $(document).on('click','.orderShowModal',function(){
        $('#orderModal'+$(this).data('order_id')).modal('show');
    });
    
});
