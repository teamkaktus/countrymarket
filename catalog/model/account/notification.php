<?php
class ModelAccountNotification extends Model {

	public function getNotifications($id) {
		$sql = "SELECT oh.`comment`, oh.`order_history_id` as id_notification, oh.date_added as date,o.date_added, o.customer_id, ".
		" EXISTS(SELECT * FROM oc_notification_view nv WHERE nv.id_notification = oh.order_history_id AND nv.id_user = o.customer_id) as viewed,".
		" CONCAT(o.invoice_prefix,o.order_id) as `order`,os.`name` as `status` ".
		" FROM oc_order_history oh".
		" LEFT JOIN oc_order o ON o.order_id = oh.order_id".
		" LEFT JOIN oc_order_status os ON os.order_status_id = oh.order_status_id ".
		" WHERE  o.customer_id =".$id.
		" AND oh.order_status_id <> 0".
		" GROUP BY id_notification".
		" ORDER BY id_notification DESC";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getNotificationsCount($id) {
		$sql = "SELECT COUNT(*) as count FROM oc_order_history oh".
		" LEFT JOIN oc_order o ON o.order_id = oh.order_id".
		" WHERE  o.customer_id =".$id.
		" AND oh.order_status_id <> 0".
		" AND NOT EXISTS(SELECT * FROM oc_notification_view nv WHERE nv.id_notification = oh.order_history_id AND nv.id_user = o.customer_id)";
		$query = $this->db->query($sql);
		return $query->rows[0]['count'];
	}
	public function setViewedNotif($viewed) {
		$sql = "INSERT INTO oc_notification_view (`id_notification`,`id_user`) VALUES ";
		$sql .=  implode(',', $viewed);
		$query = $this->db->query($sql);
	}
}
