<?php

class ControllerAccountNotification extends Controller
{
    public function index()
    {
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/edit', '', 'SSL');

            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }
        $this->load->language('account/order');
        $data['decoration']   = $this->language->get('decoration');
        $data['posted']       = $this->language->get('posted');
        $data['delivered']    = $this->language->get('delivered');
        $data['decorated']    = $this->language->get('decorated');
        $data['text_empty_not']    = $this->language->get('text_empty_not');

        $this->load->language('account/notification');

        $data['heading_title'] = $this->language->get('heading_title');
        $data['heading_title1'] = $this->language->get('heading_title1');
        $data['heading_zam'] = $this->language->get('heading_zam');
        $data['heading_spov'] = $this->language->get('heading_spov');
        $data['heading_kyp'] = $this->language->get('heading_kyp');


        $data['status'] = $this->language->get('status');
        $data['vid'] = $this->language->get('vid');
        $data['zmin'] = $this->language->get('zmin');
        $data['com'] = $this->language->get('com');
        $data['comvid'] = $this->language->get('comvid');

        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('account/notification');

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', 'SSL'),
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_spov'),
            'href' => $this->url->link('account/notification', '', 'SSL'),
        );

        $data['heading_title'] = 'Сповіщення';
				$customer_id = $this->customer->getId();
        $notifications = $this->model_account_notification->getNotifications($customer_id);
        $data['notifications'] = $notifications;
        $viewed = [];
        foreach ($notifications as $notification) {
						if(!$notification['viewed']){
							$id_notification = $notification['id_notification'];
							$viewed[] = "('$id_notification','$customer_id')";
						}
        }
				$data['count'] = count($viewed);
				if (count($viewed)) {
					$this->model_account_notification->setViewedNotif($viewed);
				}
        $data['back'] = $this->url->link('account/account', '', 'SSL');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE.$this->config->get('config_template').'/template/account/notification.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template').'/template/account/notification.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/account/notification.tpl', $data));
        }
    }
}
