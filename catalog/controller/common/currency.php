<?php
class ControllerCommonCurrency extends Controller {
	public function index() {
		$this->load->language('common/currency');

		$data['text_currency'] = $this->language->get('text_currency');

		$data['action'] = $this->url->link('common/currency/currency', '', $this->request->server['HTTPS']);

		$data['code'] = $this->currency->getCode();

		$this->load->model('localisation/currency');

		$data['currencies'] = array();

		$results = $this->model_localisation_currency->getCurrencies();




		function _is_curl_installed() {
        if  (in_array  ('curl', get_loaded_extensions())) {
        return true;
        }
        else {
        return false;
        }
        }

//         global $dna; $dna = true;
//         if ( _is_curl_installed() ){
//         $url = "https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=5";
//         $curl = curl_init($url);
//         if ( $curl ){
//         curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
//
//         $page = curl_exec($curl);
//
//         curl_close($curl);
//         unset($curl);
//
//         $xml = new SimpleXMLElement($page);
//         $USD = ($xml->row[2]->exchangerate['buy'][0]);
//         $EUR = ($xml->row[0]->exchangerate['buy'][0]);
//         $RUB = ($xml->row[1]->exchangerate['buy'][0]);
//         }
//         }

         $USD = '5';
         $EUR = '4';
         $RUB = '2';
/*
		foreach ($results as $result) {
			if ($result['status']) {
				$data['currencies'][] = array(
					'title'        => $result['title'],
					'code'         => $result['code'],
					'symbol_left'  => $result['symbol_left'],
					'symbol_right' => $result['symbol_right'],
					'value' 	   => $result['value']
				);
			}
		}
*/


		$data['currencies'][0]['value']= (float)$USD;
		$data['currencies'][0]['symbol_left']= '<span class="usd">USD:</span> ';

		$data['currencies'][1]['value']= (float)$EUR;
		$data['currencies'][1]['symbol_left']= '<span class="eur">EUR:</span> ';

		$data['currencies'][2]['value']= (float)$RUB;
		$data['currencies'][2]['symbol_left']= '<span class="rub">RUB:</span> ';



		if (!isset($this->request->get['route'])) {
			$data['redirect'] = $this->url->link('common/home');
		} else {
			$url_data = $this->request->get;

			unset($url_data['_route_']);

			$route = $url_data['route'];

			unset($url_data['route']);

			$url = '';

			if ($url_data) {
				$url = '&' . urldecode(http_build_query($url_data, '', '&'));
			}

			$data['redirect'] = $this->url->link($route, $url, $this->request->server['HTTPS']);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/currency.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/currency.tpl', $data);
		} else {
			return $this->load->view('default/template/common/currency.tpl', $data);
		}
	}

	public function currency() {
		if (isset($this->request->post['code'])) {
			$this->currency->set($this->request->post['code']);

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
		}

		if (isset($this->request->post['redirect'])) {
			$this->response->redirect($this->request->post['redirect']);
		} else {
			$this->response->redirect($this->url->link('common/home'));
		}
	}
}