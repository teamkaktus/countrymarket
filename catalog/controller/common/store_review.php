<?php
class ControllerCommonStoreReview extends Controller {
	public function index() {
		$this->load->language('review/store_review');
		$this->load->language('common/store_review');
        $this->load->model('review/store_review');

        $data['text_no_reviews'] = $this->language->get('text_no_reviews');
        $data['reviews_about_us'] = $this->language->get('reviews_about_us');

      
        $page = 1;
    

        $data['reviews'] = array();

        $review_total = $this->model_review_store_review->getTotalReviews();

        $results = $this->model_review_store_review->getReviews(($page - 1) * 5, 5);
        $this->load->model('account/customer');
        foreach ($results as $result) {
            if($result['customer_id']==0){
              $reviewimage = 'image/vidgyk.png';  
            }else{
                $customer = $this->model_account_customer->getCustomer($result['customer_id']);
                 $reviewimage = 'image/catalog/avatars/'.$customer['photo'];  
            }


            $data['reviews'][] = array(
                'review_id'  => $result['review_id'],
                'image'      => $reviewimage,
                'author'     => $result['author'],
                'text'       => utf8_substr(strip_tags(html_entity_decode($result['text'], ENT_QUOTES, 'UTF-8')), 0, 180) . '',
                'rating'     => (int)$result['rating'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
            );
        }
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/store_review.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/store_review.tpl', $data);
		} else {
			return $this->load->view('default/template/common/store_review.tpl', $data);
		}
	}
}