<?php
class ControllerCommonHeader extends Controller {
	public function index() {
		// Analytics
            $timezone  = +2;
            $kyiv = gmdate("Y-m-j H:i:s", time() + 3600*($timezone+date("I")));            
            $data['kyiv_date'] = $kyiv;
//            $timezone  = +2;
//            $kyiv = gmdate("Y-m-j H:i:s", time() + 3600*($timezone+date("I")));            
            
		$this->load->model('extension/extension');
		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('analytics/' . $analytic['code']);
			}
		}
		$this->document->addScript("catalog/view/javascript/jquery/owl.carousel.2.0.0-beta.2.4/owl.carousel.min.js");
		$this->document->addStyle("catalog/view/javascript/jquery/owl.carousel.2.0.0-beta.2.4/assets/owl.carousel.css");

		if ($this->request->server['HTTPS']) {
			$server = HTTPS_SERVER;
		} else {
			$server = HTTP_SERVER;
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');

		$data['text_home'] = $this->language->get('text_home');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');
		$data['language_selection'] = $this->language->get('language_selection');
		$data['select_country'] = $this->language->get('select_country');
		$data['delivery'] = $this->language->get('delivery');
		$data['about_us'] = $this->language->get('about_us');
		$data['actions'] = $this->language->get('actions');
		$data['contacts'] = $this->language->get('contacts');
		$data['reviews'] = $this->language->get('reviews');
		$data['offers'] = $this->language->get('offers');
		$data['payment'] = $this->language->get('payment');
		$data['quality_control'] = $this->language->get('quality_control');
		$data['menu'] = $this->language->get('menu');
		$data['header_profile_text'] = $this->language->get('header_profile_text');



		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['register'] = $this->url->link('account/register', '', 'SSL');
		$data['login'] = $this->url->link('account/login', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$data['download'] = $this->url->link('account/download', '', 'SSL');
		$data['logout'] = $this->url->link('account/logout', '', 'SSL');
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');


		$this->load->model('account/activity');
		$activity_data = array(
			'customer_id' => $this->customer->getId(),
			'firstname'   => $this->customer->getFirstName(),
			'mail'        => $this->customer->getEmail(),
		);
		if (isset($this->request->post['name'])) {
			$data['firstname'] = $this->request->post['firstname'];
		} elseif (!empty($activity_data)) {
			$data['firstname'] = $activity_data['firstname'];
		} else {
			$data['firstname'] = '';
		}
		if (strlen($activity_data['firstname']) == 0){
			$parts = explode("@", $activity_data['mail']);
			$data['firstname'] = $parts[0];
}
		if (isset($this->request->server['HTTP_USER_AGENT'])) {
			$robots = explode("\n", str_replace(array("\r\n", "\r"), "\n", trim($this->config->get('config_robots'))));

			foreach ($robots as $robot) {
				if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
					$status = false;

					break;
				}
			}
		}
		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($category_info)) {
			$data['image'] = $category_info['image'];
		} else {
			$data['image'] = '';
		}
		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($category_info) && is_file(DIR_IMAGE . $category_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($category_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}
		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');


		$this->load->model('setting/store');
		$data['stores'] = array();
		$stores = $this->model_setting_store->getStores();
		$home_url = '';
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$home_url  = $this->config->get('config_ssl');
		} else {
			$home_url  = $this->config->get('config_url');
		}
		$default_stor_name = $this->language->get('default_stor_name');
		$data['current_store'] = (int)$this->config->get('config_store_id');
		$data['stores'][] = array(
				'store_id' => '0',
				'name'	   => $default_stor_name,
				'href' 	   => '/'
			);
		foreach ($stores as $store) {
			$data['stores'][] = array(
				'store_id' => $store['store_id'],
				'name'	   => $store['name'],
				'href' 	   => $store['url']
			);
		}

		$data['categories'] = array();
		$categories_1 = $this->model_catalog_category->getCategories(0);

		foreach ($categories_1 as $category_1) {
			$level_2_data = array();

			$categories_2 = $this->model_catalog_category->getCategories($category_1['category_id']);

			foreach ($categories_2 as $category_2) {
				$level_3_data = array();

				$categories_3 = $this->model_catalog_category->getCategories($category_2['category_id']);

				foreach ($categories_3 as $category_3) {
					$level_3_data[] = array(
						'category_id' => $category_3['category_id'],
						'name'        => $category_3['name'],
						'href' => $this->url->link('product/category', 'path=' . $category_1['category_id'] .'_'.$category_2['category_id'] .'_'.$category_2['category_id'])
					);
				}

				$level_2_data[] = array(
					'category_id' => $category_2['category_id'],
					'name'  => $category_2['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($level_3_data) . ')' : ''),
					'children'    => $level_3_data,
					'href'  => $this->url->link('product/category', 'path=' . $category_1['category_id'] . '_' . $category_2['category_id'])
				);
			}
			$data['categories'][] = array(
				'category_id' => $category_1['category_id'],
				'name'     => $category_1['name'],
				'image'    => $category_1['image'],
				'children' => $level_2_data,
				'column'   => $category_1['column'] ? $category_1['column'] : 1,
				'href'     => $this->url->link('product/category', 'path=' . $category_1['category_id'].'#'. $category_1['category_id'])
			);
		}
		$categories = $this->model_catalog_category->getCategories(0);
                foreach ($categories as $category) {
                    if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1

                    }
		}
		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');
        

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
			} elseif (isset($this->request->get['path'])) {
				$parts = explode('_', (string)$this->request->get['path']);

				$category_id = (int)array_pop($parts);
				$class = '-' . $this->request->get['path'];
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
			$data['showcart']='0';
			if(($this->request->get['route']=='common/home') || ($this->request->get['route']=='product/search')|| ($this->request->get['route']=='product/category')|| ($this->request->get['route']=='product/product')){
				$data['showcart']='1';
			}

		} else {
			$data['class'] = 'common-home';
			$data['showcart']='1';
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/header.tpl', $data);
		} else {
			return $this->load->view('default/template/common/header.tpl', $data);
		}
	}
        
	public function getdatatimezone() {
            $timezone  = $_POST['timezone'];
            $date = gmdate("Y-m-j H:i:s", time() + 3600*($timezone+date("I")));            
            $result['date'] = $date;
            echo json_encode($result);
	}
}
