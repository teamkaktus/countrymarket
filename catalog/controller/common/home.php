<?php
class ControllerCommonHome extends Controller {
	public function index() {

		$this->load->language('common/home');

		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
        $data['store_review'] = $this->load->controller('common/store_review');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['cart'] = $this->load->controller('common/cart');

		$data['more_shares'] = $this->language->get('more_shares');
		$data['the_best_range'] = $this->language->get('the_best_range');
		$data['healthy_eating'] = $this->language->get('healthy_eating');
		$data['loyalty_program'] = $this->language->get('loyalty_program');



		if (isset($this->request->post['photo'])) {
			$data['photo'] = $this->request->post['photo'];
		} elseif (!empty($customer_info)) {
			$data['photo'] = $customer_info['photo'];
		} else {
			$data['photo'] = '';
		}




		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
		}
	}
}