<?php
class ControllerModuleFeatured2 extends Controller {
	public function index($setting) {
		$this->load->language('module/featured2');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');
                $this->load->model('extension/module');
		$this->load->model('tool/image');

		$data['products'] = array();
                $product_id = '';
                if(!empty($setting['product_id'])){
                    if($setting['dateChange'] == date("j.n.Y")){
                        $product_id = $setting['product_id'];              
                    }else{
                        $category_id = 1;
                        if(!empty($setting['category_id'])){
                            $category_id = $setting['category_id'];
                        }else{
                            $setting['category_id'] = 1;
                        }
                        $moduleProduct = $this->model_catalog_product->getProductscategory($category_id);
                        $setting['dateChange'] = date("j.n.Y");
                        $setting['product_id'] = $moduleProduct['product_id'];
                        $this->model_extension_module->editModule('featured2', $setting);
                        
                        $product_id = $setting['product_id'];
                    }
                }else{
                        $category_id = 1;
                        if(!empty($setting['category_id'])){
                            $category_id = $setting['category_id'];
                        }else{
                            $setting['category_id'] = 1;
                        }
                        $moduleProduct = $this->model_catalog_product->getProductscategory($category_id);
                        $setting['dateChange'] = date("j.n.Y");
                        $setting['product_id'] = $moduleProduct['product_id'];
                        $this->model_extension_module->editModule('featured2', $setting);
                        
                        $product_id = $setting['product_id'];
                }
                
                $product_info = $this->model_catalog_product->getProduct($product_id);
                if ($product_info) {
                        if ($product_info['image']) {
                                $image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
                        } else {
                                $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                        }

                        if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                                $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), '', '', true, 'right');
                        } else {
                                $price = false;
                        }

                        if ((float)$product_info['special']) {
                                $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), '', '', true, 'right');
                        } else {
                                $special = false;
                        }

                        if ($this->config->get('config_tax')) {
                                $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
                        } else {
                                $tax = false;
                        }

                        if ($this->config->get('config_review_status')) {
                                $rating = $product_info['rating'];
                        } else {
                                $rating = false;
                        }

                        $data['products'][] = array(
                                'product_id'  => $product_info['product_id'],
                                'thumb'       => $image,
                                'name'        => $product_info['name'],
                                'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                                'price'       => $price,
                                'special'     => $special,
                                'tax'         => $tax,
                                'rating'      => $rating,
                                'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                        );
                }

		if ($data['products']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured2.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/featured2.tpl', $data);
			} else {
				return $this->load->view('default/template/module/featured2.tpl', $data);
			}
		}
	}
}